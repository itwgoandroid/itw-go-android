package com.j2ptechnologies.itwtravelgo.Utils;

import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;

/**
 * Created by Lenovo on 4/30/2018.
 */

public class CommonMethods extends AppCompatActivity{

    public boolean validateEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }
}
