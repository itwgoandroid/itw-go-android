package com.j2ptechnologies.itwtravelgo.Utils;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;

import com.j2ptechnologies.itwtravelgo.R;
import com.j2ptechnologies.itwtravelgo.db.DatabaseHandler;
import com.j2ptechnologies.itwtravelgo.model.DBSaveModel;
import com.j2ptechnologies.itwtravelgo.model.NotificationDetailsModel;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Lenovo on 4/20/2018.
 */

public class CommonMethodsFragments extends Fragment {


    public static void addDetails(String key, String value, DatabaseHandler db)
    {
        if(db.isDataExist(key))
        {
            db.deleteDetails(key);
            byte[] data = value.getBytes(StandardCharsets.UTF_8);
            db.addDetails(key, Base64.encodeToString(data,Base64.DEFAULT));
        }else
        {
            byte[] data = value.getBytes(StandardCharsets.UTF_8);
            db.addDetails(key, Base64.encodeToString(data,Base64.DEFAULT));
        }

    }

    public static void deleteNotification(int key, DatabaseHandler db)
    {

            db.deleteNotification(key);

    }

    public  void addDetailsForNotification(String title, String message,  String imageUrl,DatabaseHandler db)
    {


        db.addNotifications(title,message,imageUrl);


    }

    public List<NotificationDetailsModel> getAllNotification(DatabaseHandler db) {
        return db.getAllNotification();
    }

    public String changeDateNew(String startTime)
    {
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd");
        Date dateValue=null;
        try {
            dateValue = input.parse(startTime);
            input.applyPattern("EEE, MMM dd, yyyy");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String MyDate = input.format(dateValue);
        return MyDate;
    }

    public static void delete(String key, DatabaseHandler db)
    {
        if(db.isDataExist(key))
        {
            db.deleteDetails(key);
        }
    }

    public static String getDetails(String key, DatabaseHandler db)
    {
        String text = "";
        try {
            DBSaveModel dbSaveModel = null;
            if(db.isDataExist(key))
            {
                dbSaveModel=db.getsavedValue(key);
            }

            byte[] data = Base64.decode(dbSaveModel.getValue(), Base64.DEFAULT);
            text = new String(data, StandardCharsets.UTF_8);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return text;
    }

    public String changeDate(String startTime)
    {
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dateValue=null;
        try {
            dateValue = input.parse(startTime);
            input.applyPattern("EEE, MMM dd, yyyy");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String MyDate = input.format(dateValue);
        return MyDate;
    }

    public static String changeDateTimeToDate(String startTime)
    {
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dateValue=null;
        try {
            dateValue = input.parse(startTime);
            input.applyPattern("yyyy-MM-dd");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String MyDate = input.format(dateValue);
        return MyDate;
    }
    public static String getMonth(String startTime)
    {
        SimpleDateFormat input = new SimpleDateFormat("EEE, MMM dd, yyyy");
        Date dateValue=null;
        try {
            dateValue = input.parse(startTime);
            input.applyPattern("MM, yyyy");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String MyDate = input.format(dateValue);
        return MyDate;
    }

    public String changeDateToTime(String startTime)
    {
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dateValue=null;
        try {
            dateValue = input.parse(startTime);
            input.applyPattern("hh:mm a");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String MyDate = input.format(dateValue);
        return MyDate;
    }
    public static String changeDateToTimeNew(String startTime)
    {
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dateValue=null;
        try {
            dateValue = input.parse(startTime);
            input.applyPattern("hh:mm a");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String MyDate = input.format(dateValue);
        return MyDate;
    }

    public String checkInternetSpeed(Context context)
    {

        String message="";
        if(Connectivity.isConnected(context))
        {
            if(!Connectivity.isConnectedFast(context))
            {
                message= "Slow Internet Speed";
            }
        }else
        {
            message= "Please check your internet connection";

        }
        return message;
    }

    public boolean checkInternetConnection(Context context)
    {
        if(Connectivity.isConnected(context))
        {
            return true;
        }else
        {
            return false;
        }
    }
}
