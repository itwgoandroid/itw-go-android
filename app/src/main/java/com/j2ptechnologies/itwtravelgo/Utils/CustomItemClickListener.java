package com.j2ptechnologies.itwtravelgo.Utils;

import android.view.View;

/**
 * Created by Lenovo on 4/12/2018.
 */

public interface  CustomItemClickListener {
    public void onItemClick(View v, int position);
}
