package com.j2ptechnologies.itwtravelgo.Utils;

import android.annotation.TargetApi;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.view.Gravity;


import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.j2ptechnologies.itwtravelgo.R;
import com.j2ptechnologies.itwtravelgo.api.GetApi;
import com.j2ptechnologies.itwtravelgo.app.App;
import com.j2ptechnologies.itwtravelgo.db.DatabaseHandler;
import com.j2ptechnologies.itwtravelgo.model.EventBusModel;

import org.json.JSONObject;

import java.io.IOException;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import de.greenrobot.event.EventBus;

import static com.j2ptechnologies.itwtravelgo.Utils.CommonMethodsFragments.getDetails;

public class MyService extends Service {
    public static final int notify = 5000;  //interval between two services(Here Service run every 5 seconds)
    int count = 0;  //number of times service is display
    private Handler mHandler = new Handler();   //run on another Thread to avoid crash
    private Timer mTimer = null;
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    @Override
    public void onCreate() {
        if (mTimer != null) // Cancel if already existed
            mTimer.cancel();
        else
            mTimer = new Timer();   //recreate new
        mTimer.scheduleAtFixedRate(new TimeDisplay(), 0, notify);
      //  Toast.makeText(this, "Service Created", Toast.LENGTH_LONG).show();

    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

       /* while (Constants.isRunning) {
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Cricbuzz c = new Cricbuzz();

                    String id = Constants.matchId;
                    Map<String,Map> score = null;
                    try {
                        score = c.commentary(id);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    Gson gson = new GsonBuilder().setPrettyPrinting().create();
                    String json = gson.toJson(score);
                    EventBus.getDefault().post(new LiveComentaryModel(json));
                }
            }, 15000);
          //  Toast.makeText(this, "Service Started", Toast.LENGTH_LONG).show();
        }*/

        return Service.START_STICKY;


    }
    @Override
    public void onDestroy() {
      //  Toast.makeText(this, "Service Stopped", Toast.LENGTH_LONG).show();

    }

    class TimeDisplay extends TimerTask {
        @Override
        public void run() {
            // run on another thread
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                       DatabaseHandler db=new DatabaseHandler(getBaseContext());
                       getTrips(db);
                }
            });

        }

    }


    public void getTrips(final DatabaseHandler db) {
        String URL = Constants.getTripUrl + getDetails("currentEmail", db);

        GetApi updateProfileApi = new GetApi(
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {

                        EventBus.getDefault().post(new EventBusModel(jsonObject.toString()));
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        }, URL);

        App.getVolleyQueue().add(updateProfileApi);

    }




}