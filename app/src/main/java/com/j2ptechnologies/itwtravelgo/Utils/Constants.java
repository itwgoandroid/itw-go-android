package com.j2ptechnologies.itwtravelgo.Utils;

/**
 * Created by Lenovo on 4/30/2018.
 */

public class Constants {

    public static String ipAddress="http://stageenv.ddns.net/api/trip/";
    public static String urlUserPath="user/";
    public static String urlTripPath="trip/getTripsByUser?email=";
    public static String loginUrl=ipAddress+urlUserPath+"getUserByEmailAndPassword";
    public static String getTripUrl=ipAddress+urlTripPath;
    public static String baseUrl="http://stageenv.ddns.net/data/";
    public static String setPassword =ipAddress+urlUserPath+"changePassword";
    public static String saveUser=ipAddress+"trip/saveUser";
    public static String defaultPassword="$itw#150!";
    public static String saveFcmId=ipAddress+"user/saveFcmId";
    public static String resetPassword=ipAddress+urlUserPath+"resetPassword?email=";
    public static int currentItem=1;



}
