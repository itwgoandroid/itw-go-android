package com.j2ptechnologies.itwtravelgo.adapter;

import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import com.j2ptechnologies.itwtravelgo.R;
import com.j2ptechnologies.itwtravelgo.db.DatabaseHandler;
import com.j2ptechnologies.itwtravelgo.model.NotificationDetailsModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.TenantViewHolder> {


    Context context;
    List<NotificationDetailsModel> rowItems;
    private List<NotificationDetailsModel> itemsPendingRemoval;

    private static final int PENDING_REMOVAL_TIMEOUT = 3000; // 3sec
    private Handler handler = new Handler(); // hanlder for running delayed runnables
    HashMap<NotificationDetailsModel, Runnable> pendingRunnables = new HashMap<>();


    public NotificationsAdapter(Context context, List<NotificationDetailsModel> rowItems) {
        this.context = context;
        this.rowItems = rowItems;
        itemsPendingRemoval = new ArrayList<>();
    }

    @Override
    public TenantViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itmView = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_list_item, parent, false);
        return new TenantViewHolder(itmView);
    }

    @Override
    public void onBindViewHolder(TenantViewHolder holder, final int position) {

        final NotificationDetailsModel row_pos = rowItems.get(position);

        if (itemsPendingRemoval.contains(row_pos)) {
            /** {show swipe layout} and {hide regular layout} */
            holder.regularLayout.setVisibility(View.GONE);
            holder.swipeLayout.setVisibility(View.VISIBLE);
            holder.undo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    undoOpt(row_pos);
                }
            });
        } else {
            /** {show regular layout} and {hide swipe layout} */
            holder.regularLayout.setVisibility(View.VISIBLE);
            holder.swipeLayout.setVisibility(View.GONE);
            if (row_pos.getImageUrl() != null && row_pos.getImageUrl().toString().length() > 0) {
                holder.notificationImage.setVisibility(View.VISIBLE);
                holder.title.setVisibility(View.VISIBLE);
                holder.title5.setVisibility(View.VISIBLE);
                holder.title.setText(row_pos.getTitle());
                holder.title5.setText(row_pos.getMessage());
                holder.title2.setVisibility(View.GONE);
                holder.title.setTextColor(Color.parseColor("#ffffff"));
                Glide.with(context).load(row_pos.getImageUrl()).into(holder.notificationImage);
                holder.defaultImage.setVisibility(View.GONE);
            } else {
                holder.notificationImage.setVisibility(View.GONE);
                holder.title.setVisibility(View.GONE);
                holder.title5.setVisibility(View.GONE);
                holder.title2.setVisibility(View.VISIBLE);
                holder.title2.setText(row_pos.getTitle());
                holder.message.setText(row_pos.getMessage());
                holder.title2.setTextColor(Color.parseColor("#0b8066"));
                holder.defaultImage.setVisibility(View.VISIBLE);
            }
        }





    }

    private void undoOpt( NotificationDetailsModel row_pos) {
        Runnable pendingRemovalRunnable = pendingRunnables.get(row_pos);
        pendingRunnables.remove(row_pos);
        if (pendingRemovalRunnable != null)
            handler.removeCallbacks(pendingRemovalRunnable);
        itemsPendingRemoval.remove(row_pos);
        // this will rebind the row in "normal" state
        notifyItemChanged(rowItems.indexOf(row_pos));
    }
    public void pendingRemoval(int position) {

        final NotificationDetailsModel row_pos = rowItems.get(position);
        if (!itemsPendingRemoval.contains(row_pos)) {
            itemsPendingRemoval.add(row_pos);
            // this will redraw row in "undo" state
            notifyItemChanged(position);
            // let's create, store and post a runnable to remove the data
            Runnable pendingRemovalRunnable = new Runnable() {
                @Override
                public void run() {
                    DatabaseHandler db=new DatabaseHandler(context);
                    db.deleteNotification(row_pos.getId());
                    remove(rowItems.indexOf(row_pos));
                }
            };
            handler.postDelayed(pendingRemovalRunnable, PENDING_REMOVAL_TIMEOUT);
            pendingRunnables.put(row_pos, pendingRemovalRunnable);
        }
    }

    public void remove(int position) {
        final NotificationDetailsModel row_pos = rowItems.get(position);
        if (itemsPendingRemoval.contains(row_pos)) {
            itemsPendingRemoval.remove(row_pos);
        }
        if (rowItems.contains(row_pos)) {
            rowItems.remove(position);
            notifyItemRemoved(position);
        }
    }

    public boolean isPendingRemoval(int position) {
        final NotificationDetailsModel row_pos = rowItems.get(position);
        return itemsPendingRemoval.contains(row_pos);
    }


    @Override
    public int getItemCount() {
        return rowItems.size();
    }

    final class TenantViewHolder extends RecyclerView.ViewHolder {

        TextView message, title, title2,title5;
        ImageView notificationImage, defaultImage;
        public LinearLayout regularLayout;
        public LinearLayout swipeLayout;

        public TextView undo;


        TenantViewHolder(View itemView) {
            super(itemView);
            message = (TextView) itemView.findViewById(R.id.message);
            title = (TextView) itemView.findViewById(R.id.title);
            title2 = (TextView) itemView.findViewById(R.id.title2);
            title5 = (TextView) itemView.findViewById(R.id.title5);
            notificationImage = itemView.findViewById(R.id.notificationImage);
            defaultImage = itemView.findViewById(R.id.defaultImage);
            regularLayout = (LinearLayout) itemView.findViewById(R.id.regularLayout);

            swipeLayout = (LinearLayout) itemView.findViewById(R.id.swipeLayout);
            undo = (TextView) itemView.findViewById(R.id.undo);


        }


    }



}