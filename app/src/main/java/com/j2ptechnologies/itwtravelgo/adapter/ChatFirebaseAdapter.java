package com.j2ptechnologies.itwtravelgo.adapter;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.j2ptechnologies.itwtravelgo.R;
import com.j2ptechnologies.itwtravelgo.Utils.CircularTextView;
import com.j2ptechnologies.itwtravelgo.Utils.FileDownloader;
import com.j2ptechnologies.itwtravelgo.model.ChatModel;
import com.j2ptechnologies.itwtravelgo.util.Util;

import java.io.File;
import java.io.IOException;

import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;


public class ChatFirebaseAdapter extends FirebaseRecyclerAdapter<ChatModel, ChatFirebaseAdapter.MyChatViewHolder> {

    private static final int RIGHT_MSG = 0;
    private static final int LEFT_MSG = 1;
    private static final int RIGHT_MSG_IMG = 2;
    private static final int LEFT_MSG_IMG = 3;
    String fileNames;
    Context mContext;

    private ClickListenerChatFirebase mClickListenerChatFirebase;

    private String nameUser;


    public ChatFirebaseAdapter(Context mContext, DatabaseReference ref, String nameUser, ClickListenerChatFirebase mClickListenerChatFirebase) {
        super(ChatModel.class, R.layout.item_message_left, ChatFirebaseAdapter.MyChatViewHolder.class, ref);
        this.nameUser = nameUser;
        this.mClickListenerChatFirebase = mClickListenerChatFirebase;
        this.mContext = mContext;
    }

    @Override
    public MyChatViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == RIGHT_MSG) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message_right, parent, false);
            return new MyChatViewHolder(view);
        } else if (viewType == LEFT_MSG) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message_left, parent, false);
            return new MyChatViewHolder(view);
        } else if (viewType == RIGHT_MSG_IMG) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message_right_img, parent, false);
            return new MyChatViewHolder(view);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message_left_img, parent, false);
            return new MyChatViewHolder(view);
        }
    }

    @Override
    public int getItemViewType(int position) {
        ChatModel model = getItem(position);
        /*if (model.getMessageType().equalsIgnoreCase("textMessage")) {

        }*/
       /* if (model.getLattidue() != null && model.getLongitude() != null) {
            if (model.getEmail().equals(nameUser)) {
                return RIGHT_MSG_IMG;
            } else {
                return LEFT_MSG_IMG;
            }
        } else*/

        if (model.getMessageType().equalsIgnoreCase("location")) {
            if (model.getEmail().equals(nameUser)) {
                return RIGHT_MSG_IMG;
            } else {
                return LEFT_MSG_IMG;
            }
        } else if (model.getMessageType().equalsIgnoreCase("media")) {
            if (model.getFileType().equals("img") && model.getEmail().equals(nameUser)) {
                return RIGHT_MSG_IMG;
            } else if (model.getFileType().equalsIgnoreCase("pdf")) {
                if (model.getEmail().equals(nameUser)) {
                    return RIGHT_MSG_IMG;
                } else {
                    return LEFT_MSG_IMG;
                }
            } else {
                return LEFT_MSG_IMG;
            }


        } else if (model.getEmail().equals(nameUser)) {
            return RIGHT_MSG;
        } else {
            return LEFT_MSG;
        }

        // return LEFT_MSG;
    }

    @Override
    protected void populateViewHolder(MyChatViewHolder viewHolder, ChatModel model, int position) {


        viewHolder.setIvUser(model.getEmail(), model);


        if (model.getMessageType().equalsIgnoreCase("textMessage")) {

            viewHolder.setTxtMessage(model.getMessage());

            viewHolder.tvIsLocation(View.GONE);
        } else if (model.getMessageType().equalsIgnoreCase("media")) {
            if (model.getFileType().equalsIgnoreCase("img")) {
                viewHolder.tvIsLocation(View.GONE);
                viewHolder.setIvChatPhoto(model.getFileUrl());
            } else if (model.getFileType().equalsIgnoreCase("pdf")) {
                viewHolder.tvIsLocation(View.GONE);
                viewHolder.setPdfPhoto();
            }
        } else if (model.getLattidue() != null && model.getLongitude() != null && !model.getFileType().equalsIgnoreCase("pdf")) {
            viewHolder.setIvChatPhoto(Util.local(model.getLattidue(), model.getLongitude()));
            viewHolder.tvIsLocation(View.VISIBLE);
        }/* else if (model.getMessageType().equalsIgnoreCase("pdf")) {
            viewHolder.tvIsLocation(View.GONE);
            viewHolder.setPdfPhoto();
        }*/

        viewHolder.setTvTimestamp(model.getTimeStamp());
    }

    public class MyChatViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvTimestamp, tvLocation;
        EmojiconTextView txtMessage;
        ImageView ivChatPhoto;
        CircularTextView ivUser;

        public MyChatViewHolder(View itemView) {
            super(itemView);
            tvTimestamp = (TextView) itemView.findViewById(R.id.timestamp);
            txtMessage = (EmojiconTextView) itemView.findViewById(R.id.txtMessage);
            tvLocation = (TextView) itemView.findViewById(R.id.tvLocation);
            ivChatPhoto = (ImageView) itemView.findViewById(R.id.img_chat);
            ivUser = (CircularTextView) itemView.findViewById(R.id.ivUserChat);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            ChatModel model = getItem(position);

            if (model.getMessageType().equalsIgnoreCase("location")) {
                mClickListenerChatFirebase.clickImageMapChat(view, position, model.getLattidue(), model.getLongitude());
            } else if (model.getMessageType().equalsIgnoreCase("media")) {
                if (model.getFileType().equalsIgnoreCase("img"))
                    mClickListenerChatFirebase.clickImageChat(view, position, model.getEmail(), null, model.getFileUrl());
                else if (model.getFileType().equalsIgnoreCase("pdf")) {
                    fileNames = model.getFileName() + ".pdf";
                    new DownloadFile().execute(model.getFileUrl(), fileNames);
                }

            } /*else if (model.getMessageType().equalsIgnoreCase("pdf")) {

            }*/

            /*if (model.getLattidue() != null && model.getLongitude() != null) {
                mClickListenerChatFirebase.clickImageMapChat(view, position, model.getLattidue(), model.getLongitude());
            } else {
                mClickListenerChatFirebase.clickImageChat(view, position, model.getEmail(), null, model.getFileUrl()
                );
            }*/
        }

        public void setTxtMessage(String message) {
            txtMessage.setVisibility(View.VISIBLE);
            if (txtMessage == null) return;
            txtMessage.setText(message);
        }

        public void setIvUser(String user, ChatModel model) {
            if (ivUser == null) return;
            //Glide.with(ivUser.getContext()).load(urlPhotoUser).centerCrop().transform(new CircleTransform(ivUser.getContext())).override(40, 40).into(ivUser);
            if (model.getEmail().equals(nameUser)) {
                ivUser.setText(user.substring(0, 1).toUpperCase());
                ivUser.setStrokeWidth(1);
                ivUser.setSolidColor("#5c8991");
                ivUser.setStrokeColor("#2e585e");
            } else {
                ivUser.setText("A");
                ivUser.setStrokeWidth(1);
                ivUser.setSolidColor("#ff0000");
                ivUser.setStrokeColor("#d10000");
            }
        }

        public void setTvTimestamp(long timestamp) {
            if (tvTimestamp == null) return;
            tvTimestamp.setText(converteTimestamp(timestamp + ""));
        }

        public void setIvChatPhoto(String url) {
            if (ivChatPhoto == null) return;
            Glide.with(ivChatPhoto.getContext()).load(url)
                    .override(100, 100)
                    .fitCenter()
                    .into(ivChatPhoto);
            ivChatPhoto.setOnClickListener(this);
        }

        public void setPdfPhoto() {
            //  ivChatPhoto.setBackground(obtainDrawable(mContext, R.drawable.file));
            ivChatPhoto.setImageDrawable(mContext.getResources().getDrawable(R.drawable.pdf_preview));
            ivChatPhoto.setOnClickListener(this);
        }

        public void tvIsLocation(int visible) {
            if (tvLocation == null) return;
            tvLocation.setVisibility(visible);
        }

    }

    private CharSequence converteTimestamp(String mileSegundos) {
        return DateUtils.getRelativeTimeSpanString(Long.parseLong(mileSegundos), System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);
    }


    /*public void download(String url, String fileName) {
        fileNames = fileName;
        new DownloadFile().execute(url, fileName);
    }*/

    public void view() {
        File pdfFile = new File(Environment.getExternalStorageDirectory() + "/ITW_Downloads/" + fileNames);  // -> filename = maven.pdf
        Uri path = Uri.fromFile(pdfFile);
        Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
        pdfIntent.setDataAndType(path, "application/pdf");
        pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        try {
            mContext.startActivity(pdfIntent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(mContext, "No Application available to view PDF", Toast.LENGTH_SHORT).show();
        }
    }

    private class DownloadFile extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... strings) {
            String fileUrl = strings[0];
            String fileName = strings[1];
            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
            File folder = new File(extStorageDirectory, "ITW_Downloads");
            folder.mkdir();

            File pdfFile = new File(folder, fileName);

            try {
                pdfFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            FileDownloader.downloadFile(fileUrl, pdfFile);
            return null;
        }

        @Override
        protected void onPreExecute() {
            Toast.makeText(mContext, "Downloading... Please wait", Toast.LENGTH_LONG).show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            view();
            super.onPostExecute(aVoid);
        }
    }

    public final Drawable obtainDrawable(Context context, int id) {
        final int version = Build.VERSION.SDK_INT;
        if (version >= 21) {
            return ContextCompat.getDrawable(context, id);
        } else {
            return context.getResources().getDrawable(id);
        }
    }

}
