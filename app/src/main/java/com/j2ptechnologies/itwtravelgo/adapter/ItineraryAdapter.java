package com.j2ptechnologies.itwtravelgo.adapter;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.j2ptechnologies.itwtravelgo.Activity.AccomodationDetailsActivity;
import com.j2ptechnologies.itwtravelgo.Activity.FlightDetailsActivity;
import com.j2ptechnologies.itwtravelgo.R;
import com.j2ptechnologies.itwtravelgo.Utils.CommonMethodsFragments;
import com.j2ptechnologies.itwtravelgo.model.ItineraryModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class ItineraryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    int id = 0;
    List<ItineraryModel> rowItems;
    private static final int TYPE_FLIGHT = 1;
    private static final int TYPE_ACCOMODATION = 2;
    private static final int TYPE_TRANSPORTATION = 3;
    private static final int TYPE_ACTIVITY = 4;
    private static final int TYPE_INSURANCE = 5;
    Context context;


    JSONObject obj;

    public ItineraryAdapter(List<ItineraryModel> rowItems, Context context) {
        this.rowItems = rowItems;
        this.context = context;
    }

    // get the size of the list
    @Override
    public int getItemCount() {
        return rowItems == null ? 0 : rowItems.size();
    }

    // determine which layout to use for the row
    @Override
    public int getItemViewType(int position) {
        ItineraryModel item = rowItems.get(position);
        if (item.getId() == 3) {
            return TYPE_TRANSPORTATION;
        } else if (item.getId() == 1) {
            return TYPE_FLIGHT;
        } else if (item.getId() == 2) {
            return TYPE_ACCOMODATION;
        } else if (item.getId() == 4) {
            return TYPE_ACTIVITY;
        } else if (item.getId() == 5) {
            return TYPE_INSURANCE;
        } else {
            return -1;
        }

    }


    // specify the row layout file and click for each row
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == TYPE_TRANSPORTATION) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.bus_list_item, parent, false);
            return new ViewHolderTransportation(view);
        } else if (viewType == TYPE_FLIGHT) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.flight_list_item, parent, false);
            return new ViewHolderFlight(view);
        } else if (viewType == TYPE_ACCOMODATION) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.bed_list_item, parent, false);
            return new ViewHolderAccomodation(view);
        } else if (viewType == TYPE_ACTIVITY) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activty_list_item, parent, false);
            return new ViewHolderActivity(view);
        } else if (viewType == TYPE_INSURANCE) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.insurance_list_item, parent, false);
            return new ViewHolderInsurance(view);
        } else {
            return null;
        }
    }

    // load data in each row element
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int listPosition) {
        switch (holder.getItemViewType()) {
            case TYPE_TRANSPORTATION:
                initLayoutTransportation((ViewHolderTransportation) holder, listPosition);
                break;
            case TYPE_FLIGHT:
                initLayoutFlight((ViewHolderFlight) holder, listPosition);
                break;
            case TYPE_ACCOMODATION:
                initLayoutAccomodation((ViewHolderAccomodation) holder, listPosition);
                break;
            case TYPE_ACTIVITY:
                initLayoutActivity((ViewHolderActivity) holder, listPosition);
                break;
            case TYPE_INSURANCE:
                initLayoutInsurance((ViewHolderInsurance) holder, listPosition);
                break;
            default:
                break;
        }
    }

    private void initLayoutTransportation(ViewHolderTransportation holder, int pos) {
        JSONObject jsonObject = rowItems.get(pos).getItenaryDetails();
        JSONObject userObject = rowItems.get(pos).getUserSpecificDetails();
        try {
            CommonMethodsFragments commonMethodsFragments = new CommonMethodsFragments();
            holder.date.setText(jsonObject.getString("startDate"));
            holder.title.setText(jsonObject.getString("name"));
            holder.departureDate.setText(commonMethodsFragments.changeDate(jsonObject.getString("startDate")));
            holder.departureTime.setText(commonMethodsFragments.changeDateToTime(jsonObject.getString("startDate")));
            holder.departureFrom.setText(jsonObject.getString("pickupLocation"));
            holder.arrivalDate.setText(commonMethodsFragments.changeDate(jsonObject.getString("endDate")));
            holder.arrivalTime.setText(commonMethodsFragments.changeDateToTime(jsonObject.getString("endDate")));
            holder.arrivalTo.setText(jsonObject.getString("dropoffLocation"));
            holder.bookingNo.setText("BOOKING #: " + jsonObject.getString("bookingNo"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void initLayoutInsurance(ViewHolderInsurance holder, int pos) {
        JSONObject jsonObject = rowItems.get(pos).getItenaryDetails();
        JSONObject userObject = rowItems.get(pos).getUserSpecificDetails();
        CommonMethodsFragments commonMethodsFragments = new CommonMethodsFragments();
        try {
            holder.provider.setText(jsonObject.getString("provider"));
            holder.validTill.setText(commonMethodsFragments.changeDate(jsonObject.getString("createdDate")) + "   " + commonMethodsFragments.changeDateToTime(jsonObject.getString("createdDate")));
            holder.policyNumber.setText(userObject.getString("policyNo"));
            holder.policyType.setText(jsonObject.getString("policyType"));
            holder.emergencyContact.setText(jsonObject.getString("emergencyContact"));

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void initLayoutFlight(final ViewHolderFlight holder, final int pos) {
        final JSONObject jsonObject = rowItems.get(pos).getItenaryDetails();
        final JSONObject userObject = rowItems.get(pos).getUserSpecificDetails();
        CommonMethodsFragments commonMethodsFragments = new CommonMethodsFragments();
        try {
            holder.type.setText("Type : " + jsonObject.getString("type"));
            holder.from.setText(jsonObject.getString("departureAirport"));
            holder.to.setText(jsonObject.getString("arrivalAirport"));
            holder.departureDate.setText(commonMethodsFragments.changeDate(jsonObject.getString("departureDate")));
            holder.departureTime.setText(commonMethodsFragments.changeDateToTime(jsonObject.getString("departureDate")));
            holder.departureFrom.setText(jsonObject.getString("departureTerminal"));
            holder.arrivalDate.setText(commonMethodsFragments.changeDate(jsonObject.getString("arrivalDate")));
            holder.arrivalTime.setText(commonMethodsFragments.changeDateToTime(jsonObject.getString("arrivalDate")));
            holder.arrivalTo.setText(jsonObject.getString("arrivalTerminal"));
            holder.bookingNo.setText("BOOKING #: " + jsonObject.getString("bookingNo"));
            holder.date.setText(jsonObject.getString("departureDate"));

            holder.card_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(context, FlightDetailsActivity.class);
                    i.putExtra("fromLocation", holder.from.getText().toString());
                    i.putExtra("jsonObject", rowItems.get(pos).getItenaryDetails().toString());
                    i.putExtra("userObject", userObject.toString());
                    try {
                        i.putExtra("type", jsonObject.getString("type"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    context.startActivity(i);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void initLayoutAccomodation(final ViewHolderAccomodation holder, final int pos) {
        final JSONObject jsonObject = rowItems.get(pos).getItenaryDetails();
        final JSONObject userObject = rowItems.get(pos).getUserSpecificDetails();
        CommonMethodsFragments commonMethodsFragments = new CommonMethodsFragments();
        try {
            holder.date.setText(jsonObject.getString("checkIn"));
            holder.title.setText(jsonObject.getString("hotelName"));
            holder.status.setText(jsonObject.getString("reservationStatus"));
            holder.checkInDate.setText(commonMethodsFragments.changeDate(jsonObject.getString("checkIn")));
            holder.checkInTime.setText(commonMethodsFragments.changeDateToTime(jsonObject.getString("checkIn")));
            holder.checkOutDate.setText(commonMethodsFragments.changeDate(jsonObject.getString("checkOut")));
            holder.checkOutTime.setText(commonMethodsFragments.changeDateToTime(jsonObject.getString("checkOut")));
            //holder.roomNumber.setText("ROOM #: "+userObject.getString("roomNo"));
            holder.bookingNumber.setText("BOOKING #: " + jsonObject.getString("bookingNo"));

            holder.card_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(context, AccomodationDetailsActivity.class);
                    try {
                        i.putExtra("lattitude", jsonObject.getDouble("lat") + "");
                        i.putExtra("longitude", jsonObject.getDouble("lng") + "");
                        i.putExtra("hotelName", jsonObject.getString("hotelName"));
                        i.putExtra("jsonObject", rowItems.get(pos).getItenaryDetails().toString());
                        i.putExtra("userObject", userObject.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    context.startActivity(i);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private void initLayoutActivity(ViewHolderActivity holder, int pos) {
        JSONObject jsonObject = rowItems.get(pos).getItenaryDetails();
        JSONObject userObject = rowItems.get(pos).getUserSpecificDetails();
        CommonMethodsFragments commonMethodsFragments = new CommonMethodsFragments();
        try {
            holder.date.setText(commonMethodsFragments.changeDate(jsonObject.getString("createdDate")));
            holder.title.setText(jsonObject.getString("activityTitle"));
            holder.name.setText(jsonObject.getString("activityName"));
            holder.location.setText(jsonObject.getString("location"));
            holder.contact.setText(jsonObject.getString("contact"));
            holder.bookingNumber.setText(jsonObject.getString("bookingNumber"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    // Static inner class to initialize the views of rows
    static class ViewHolderTransportation extends RecyclerView.ViewHolder {
        public TextView title;
        public TextView departureDate;
        public TextView departureTime;
        public TextView departureFrom;
        public TextView arrivalDate;
        public TextView arrivalTime;
        public TextView arrivalTo;
        public TextView bookingNo;
        public TextView date;

        public ViewHolderTransportation(View itemView) {
            super(itemView);
            date = (TextView) itemView.findViewById(R.id.date);
            title = (TextView) itemView.findViewById(R.id.title);
            departureDate = (TextView) itemView.findViewById(R.id.departureDate);
            departureTime = (TextView) itemView.findViewById(R.id.departureTime);
            departureFrom = (TextView) itemView.findViewById(R.id.departureFrom);
            arrivalDate = (TextView) itemView.findViewById(R.id.arrivalDate);
            arrivalTime = (TextView) itemView.findViewById(R.id.arrivalTime);
            arrivalTo = (TextView) itemView.findViewById(R.id.arrivalTo);
            bookingNo = (TextView) itemView.findViewById(R.id.bookingNo);
        }
    }

    static class ViewHolderFlight extends RecyclerView.ViewHolder {

        public CardView card_view;
        public TextView from;
        public TextView to;
        public TextView type;
        public TextView departureDate;
        public TextView departureTime;
        public TextView departureFrom;
        public TextView arrivalDate;
        public TextView arrivalTime;
        public TextView arrivalTo;
        public TextView bookingNo;
        public TextView date;

        public ViewHolderFlight(View itemView) {
            super(itemView);

            card_view = (CardView) itemView.findViewById(R.id.card_view);
            from = (TextView) itemView.findViewById(R.id.from);
            to = (TextView) itemView.findViewById(R.id.to);
            departureDate = (TextView) itemView.findViewById(R.id.departureDate);
            departureTime = (TextView) itemView.findViewById(R.id.departureTime);
            departureFrom = (TextView) itemView.findViewById(R.id.departureFrom);
            arrivalDate = (TextView) itemView.findViewById(R.id.arrivalDate);
            arrivalTime = (TextView) itemView.findViewById(R.id.arrivalTime);
            arrivalTo = (TextView) itemView.findViewById(R.id.arrivalTo);
            bookingNo = (TextView) itemView.findViewById(R.id.bookingNo);
            date = (TextView) itemView.findViewById(R.id.date);
            type = (TextView) itemView.findViewById(R.id.type);
        }

    }

    static class ViewHolderAccomodation extends RecyclerView.ViewHolder {
        public CardView card_view;
        public TextView date;
        public TextView title;
        public TextView status;
        public TextView checkInDate;
        public TextView checkInTime;
        public TextView checkOutDate;
        public TextView checkOutTime;
        // public TextView roomNumber;
        public TextView bookingNumber;

        public ViewHolderAccomodation(View itemView) {
            super(itemView);
            card_view = (CardView) itemView.findViewById(R.id.card_view);
            date = (TextView) itemView.findViewById(R.id.date);
            title = (TextView) itemView.findViewById(R.id.title);
            status = (TextView) itemView.findViewById(R.id.status);
            checkInDate = (TextView) itemView.findViewById(R.id.checkInDate);
            checkInTime = (TextView) itemView.findViewById(R.id.checkInTime);
            checkOutDate = (TextView) itemView.findViewById(R.id.checkOutDate);
            checkOutTime = (TextView) itemView.findViewById(R.id.checkOutTime);
            // roomNumber = (TextView) itemView.findViewById(R.id.roomNumber);
            bookingNumber = (TextView) itemView.findViewById(R.id.bookingNumber);
        }
    }

    static class ViewHolderActivity extends RecyclerView.ViewHolder {
        public TextView title;
        public TextView name;
        public TextView date;
        public TextView location;
        public TextView contact;
        public TextView bookingNumber;

        public ViewHolderActivity(View itemView) {
            super(itemView);
            date = (TextView) itemView.findViewById(R.id.date);
            name = (TextView) itemView.findViewById(R.id.name);
            location = (TextView) itemView.findViewById(R.id.location);
            contact = (TextView) itemView.findViewById(R.id.contact);
            title = (TextView) itemView.findViewById(R.id.title);
            bookingNumber = (TextView) itemView.findViewById(R.id.bookingNumber);
        }
    }

    static class ViewHolderInsurance extends RecyclerView.ViewHolder {
        public TextView provider;
        public TextView validTill;
        public TextView policyNumber;
        public TextView policyType;
        public TextView emergencyContact;

        public ViewHolderInsurance(View itemView) {
            super(itemView);
            provider = (TextView) itemView.findViewById(R.id.provider);
            validTill = (TextView) itemView.findViewById(R.id.validTill);
            policyNumber = (TextView) itemView.findViewById(R.id.policyNumber);
            policyType = (TextView) itemView.findViewById(R.id.policyType);
            emergencyContact = (TextView) itemView.findViewById(R.id.emergencyContact);
        }
    }
}