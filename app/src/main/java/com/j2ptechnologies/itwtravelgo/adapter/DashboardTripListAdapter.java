package com.j2ptechnologies.itwtravelgo.adapter;

import android.support.v4.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.j2ptechnologies.itwtravelgo.Activity.TripDetailsActivity;
import com.j2ptechnologies.itwtravelgo.R;
import com.j2ptechnologies.itwtravelgo.Utils.CatLoadingView;
import com.j2ptechnologies.itwtravelgo.Utils.CommonMethodsFragments;
import com.j2ptechnologies.itwtravelgo.db.DatabaseHandler;
import com.j2ptechnologies.itwtravelgo.model.DashboardTripsListModel;

import java.util.ArrayList;

/**
 * Created by Lenovo on 4/22/2018.
 */

public class DashboardTripListAdapter extends RecyclerView.Adapter<DashboardTripListAdapter.ViewHolder> {
    private ArrayList<DashboardTripsListModel> row_item;
    private Context context;
    DatabaseHandler db;
    CatLoadingView mView;

    public DashboardTripListAdapter(Context context,ArrayList<DashboardTripsListModel> android_versions,DatabaseHandler db) {
        this.context = context;
        this.row_item = android_versions;
        this.db=db;

    }

    @Override
    public DashboardTripListAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.dashboard_list_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int i) {
        final int newi=i;

        viewHolder.name.setText(row_item.get(i).getName());
        viewHolder.startAndEndDate.setText(row_item.get(i).getStartDate() +" - "+row_item.get(i).getEndDate());
        Glide.with(context).load(row_item.get(i).getImageUrl()).into(viewHolder.image);
        mView = new CatLoadingView();
        viewHolder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDialog();


                Handler handler=new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        dismissDialog();
                        CommonMethodsFragments commonMethodsFragments =new CommonMethodsFragments();
                        commonMethodsFragments.addDetails("currentTripId",row_item.get(i).getId(),db);
                        commonMethodsFragments.addDetails("currentTripUserObject",row_item.get(i).getCurrentTripUserObject().toString(),db);
                        commonMethodsFragments.addDetails("currentDocumentPath",row_item.get(i).getDocumentPath(),db);
                        commonMethodsFragments.addDetails("currentTripDate",row_item.get(i).getStartDate() +" - "+row_item.get(i).getEndDate(),db);
                        commonMethodsFragments.addDetails("tripStartMonth",row_item.get(i).getStartDate(),db);
                        commonMethodsFragments.addDetails("tripEndMonth",row_item.get(i).getEndDate(),db);
                        Intent i=new Intent(context,TripDetailsActivity.class);
                        i.putExtra("imageUrl",row_item.get(newi).getImageUrl());
                        context.startActivity(i);
                    }
                },2000);



            }
        });
}

    @Override
    public int getItemCount() {
        return row_item.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView startAndEndDate;
        TextView name;
        ImageView image;
        public ViewHolder(View view) {
            super(view);

            name = (TextView)view.findViewById(R.id.name);
            startAndEndDate = (TextView)view.findViewById(R.id.startAndEndDate);
            image = (ImageView)view.findViewById(R.id.image);
        }
    }

    public void showDialog() {
        mView.setClickCancelAble(false);
        FragmentManager manager = ((AppCompatActivity)context).getSupportFragmentManager();
        mView.show(manager, "");
    }
    public void dismissDialog() {
        mView.dismiss();
    }
}