package com.j2ptechnologies.itwtravelgo.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import com.j2ptechnologies.itwtravelgo.model.MapSearchModel;
import com.j2ptechnologies.itwtravelgo.R;
import com.j2ptechnologies.itwtravelgo.Utils.CustomItemClickListener;

public class MapSearchAdapter extends RecyclerView.Adapter<MapSearchAdapter.ViewHolder> {
    private ArrayList<MapSearchModel> android;
    private Context context;

    CustomItemClickListener listener;




    public MapSearchAdapter(Context context,ArrayList<MapSearchModel> android, CustomItemClickListener listener) {
        this.android = android;
        this.context = context;
        this.listener=listener;
    }

    @Override
    public MapSearchAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.map_search_list_item, viewGroup, false);
        final ViewHolder mViewHolder = new ViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(v, mViewHolder.getAdapterPosition());
            }
        });
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(MapSearchAdapter.ViewHolder viewHolder, int i) {

        viewHolder.title.setText(android.get(i).getTitle());
        viewHolder.image.setImageDrawable(context.getResources().getDrawable(android.get(i).getImage()));
    }

    @Override
    public int getItemCount() {
        return android.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView title;
        private ImageView image;
        public ViewHolder(View view) {
            super(view);

            title = (TextView)view.findViewById(R.id.title);
            image = (ImageView) view.findViewById(R.id.image);
        }
    }

}
