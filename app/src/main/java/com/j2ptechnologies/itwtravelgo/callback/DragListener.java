package com.j2ptechnologies.itwtravelgo.callback;

/**
 * Created by yarolegovich on 25.03.2017.
 */

public interface DragListener {

    void onDrag(float progress);
}
