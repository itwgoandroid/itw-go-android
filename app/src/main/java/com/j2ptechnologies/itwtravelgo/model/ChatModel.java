package com.j2ptechnologies.itwtravelgo.model;


/**
 * Created by Alessandro Barreto on 17/06/2016.
 */
public class ChatModel {

   /* private String id;
    private UserModel userModel;
    private String message;
    private String timeStamp;
    private FileModel file;
    private MapModel mapModel;

    public ChatModel() {
    }

    public ChatModel(UserModel userModel, String message, String timeStamp, FileModel file) {
        this.userModel = userModel;
        this.message = message;
        this.timeStamp = timeStamp;
        this.file = file;
    }

    public ChatModel(UserModel userModel, String timeStamp, MapModel mapModel) {
        this.userModel = userModel;
        this.timeStamp = timeStamp;
        this.mapModel = mapModel;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public UserModel getUserModel() {
        return userModel;
    }

    public void setUserModel(UserModel userModel) {
        this.userModel = userModel;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public FileModel getFile() {
        return file;
    }

    public void setFile(FileModel file) {
        this.file = file;
    }

    public MapModel getMapModel() {
        return mapModel;
    }

    public void setMapModel(MapModel mapModel) {
        this.mapModel = mapModel;
    }

    @Override
    public String toString() {
        return "ChatModel{" +
                "mapModel=" + mapModel +
                ", file=" + file +
                ", timeStamp='" + timeStamp + '\'' +
                ", message='" + message + '\'' +
                ", userModel=" + userModel +
                '}';
    }*/

    private String messageType, email;
    private String lattidue, longitude;
    private String fileType;
    private String fileName;
    private String fileUrl;
    private String message;
    private String tripId;
    private long timeStamp;

    public ChatModel() {

    }

    public ChatModel(String messageType, String lattidue, String longitude, String email, String fileType, String fileName, String fileUrl, String message, String tripId, long timeStamp) {
        this.messageType = messageType;
        this.lattidue = lattidue;
        this.longitude = longitude;
        this.email = email;
        this.fileType = fileType;
        this.fileName = fileName;
        this.fileUrl = fileUrl;
        this.message = message;
        this.tripId = tripId;
        this.timeStamp = timeStamp;
    }


    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getLattidue() {
        return lattidue;
    }

    public void setLattidue(String lattidue) {
        this.lattidue = lattidue;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    @Override
    public String toString() {
        //if (getMessageType().equals("media")) {

        return "ChatModel{" +

                //", file=" + file +
                ", email='" + email +
                ", fileName=" + fileName +
                ", fileType=" + fileType +
                ", fileUrl='" + fileUrl +
                ", lattidue=" + lattidue +
                ", longitude=" + longitude +
                ", message='" + message +
                ", messageType=" + messageType +
                ", timeStamp=" + timeStamp +
                ", tripId=" + tripId +
                '}';
        /*} else
            return null;*/

    }
}
