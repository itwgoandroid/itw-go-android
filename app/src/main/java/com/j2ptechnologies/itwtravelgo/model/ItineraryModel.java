package com.j2ptechnologies.itwtravelgo.model;

import org.json.JSONObject;

/**
 * Created by Lenovo on 4/3/2018.
 */

public class ItineraryModel {

    int id;
    JSONObject itenaryDetails;
    JSONObject userSpecificDetails;

    public ItineraryModel(int id, JSONObject itenaryDetails, JSONObject userSpecificDetails) {
        this.id = id;
        this.itenaryDetails = itenaryDetails;
        this.userSpecificDetails = userSpecificDetails;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public JSONObject getItenaryDetails() {
        return itenaryDetails;
    }

    public void setItenaryDetails(JSONObject itenaryDetails) {
        this.itenaryDetails = itenaryDetails;
    }

    public JSONObject getUserSpecificDetails() {
        return userSpecificDetails;
    }

    public void setUserSpecificDetails(JSONObject userSpecificDetails) {
        this.userSpecificDetails = userSpecificDetails;
    }
}
