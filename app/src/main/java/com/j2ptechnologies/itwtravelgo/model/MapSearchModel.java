package com.j2ptechnologies.itwtravelgo.model;

/**
 * Created by Lenovo on 4/9/2018.
 */

public class MapSearchModel {
    int image;
    String title;
    String searchName;

    public MapSearchModel(int image, String title, String searchName) {
        this.image = image;
        this.title = title;
        this.searchName = searchName;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSearchName() {
        return searchName;
    }

    public void setSearchName(String searchName) {
        this.searchName = searchName;
    }
}
