package com.j2ptechnologies.itwtravelgo.model;

import org.json.JSONObject;

/**
 * Created by Lenovo on 4/22/2018.
 */

public class DashboardTripsListModel {
    String id;
    String name;
    String startDate;
    String endDate;
    String imageUrl;
    String documentPath;
    JSONObject currentTripUserObject;

    public DashboardTripsListModel(String id, String name, String startDate, String endDate, String imageUrl, JSONObject currentTripUserObject,String documentPath) {
        this.id = id;
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.imageUrl = imageUrl;
        this.currentTripUserObject = currentTripUserObject;
        this.documentPath=documentPath;
    }

    public String getDocumentPath() {
        return documentPath;
    }

    public void setDocumentPath(String documentPath) {
        this.documentPath = documentPath;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public JSONObject getCurrentTripUserObject() {
        return currentTripUserObject;
    }

    public void setCurrentTripUserObject(JSONObject currentTripUserObject) {
        this.currentTripUserObject = currentTripUserObject;
    }
}
