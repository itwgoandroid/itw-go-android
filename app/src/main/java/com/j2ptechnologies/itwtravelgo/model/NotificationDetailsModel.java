package com.j2ptechnologies.itwtravelgo.model;

/**
 * Created by shrikant.korigeri on 25-04-2018.
 */

public class NotificationDetailsModel {
    int id;
    String title;
    String message;
    String imageUrl;



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
