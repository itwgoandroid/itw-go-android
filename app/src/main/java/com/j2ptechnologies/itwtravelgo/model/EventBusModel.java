package com.j2ptechnologies.itwtravelgo.model;

import org.json.JSONObject;

/**
 * Created by Lenovo on 5/10/2018.
 */

public class EventBusModel {
    String json;

    public EventBusModel(String json) {
        this.json = json;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }
}
