package com.j2ptechnologies.itwtravelgo.model;

/**
 * Created by jeethendra.bn on 12-02-2018.
 */

public class DBSaveModel {

    String name;
    String value;


    public DBSaveModel(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
