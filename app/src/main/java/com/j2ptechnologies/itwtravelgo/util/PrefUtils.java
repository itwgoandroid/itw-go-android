package com.j2ptechnologies.itwtravelgo.util;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class PrefUtils {



    public  static void setDefaults(String key, String value, Context context) {
        SharedPreferences kicPreference = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = kicPreference.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String getDefaults(String key, Context context) {
        SharedPreferences kicPreference = PreferenceManager.getDefaultSharedPreferences(context);
        return kicPreference.getString(key, null);
    }
}
