package com.j2ptechnologies.itwtravelgo.api;

import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

/**
 * Created by vijay on 3/6/15.
 */
public class PostApi extends JsonObjectRequest {

    Response.Listener<JSONObject> listener;

    public PostApi(int method, String url, JSONObject jsonRequest, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(method, url, jsonRequest, listener, errorListener);

        this.listener = listener;
        setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }



    @Override
    protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
        Log.e("", "PUT OTP Response : " + super.parseNetworkResponse(response));
        return super.parseNetworkResponse(response);
    }

    @Override
    protected void deliverResponse(JSONObject response) {
        //super.deliverResponse(response);
        listener.onResponse(response);
    }

    @Override
    protected VolleyError parseNetworkError(VolleyError volleyError) {
        Log.e("","PUT OTP Volley Error : " + volleyError);
        return super.parseNetworkError(volleyError);
    }


}
