package com.j2ptechnologies.itwtravelgo.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class IataHelper
{
    protected static final String TAG = "DataAdapter";

    private final Context mContext;
    private SQLiteDatabase mDb;
    private DataBaseHelper mDbHelper;

    public IataHelper(Context context)
    {
        this.mContext = context;
        mDbHelper = new DataBaseHelper(mContext);
    }

    public IataHelper createDatabase() throws SQLException
    {
        try
        {
            mDbHelper.createDataBase();
        }
        catch (IOException mIOException)
        {
            Log.e(TAG, mIOException.toString() + "  UnableToCreateDatabase");
            throw new Error("UnableToCreateDatabase");
        }
        return this;
    }

    public IataHelper open() throws SQLException
    {
        try
        {
            mDbHelper.openDataBase();
            mDbHelper.close();
            mDb = mDbHelper.getReadableDatabase();
        }
        catch (SQLException mSQLException)
        {
            Log.e(TAG, "open >>"+ mSQLException.toString());
            throw mSQLException;
        }
        return this;
    }

    public void close()
    {
        mDbHelper.close();
    }

    public Cursor getTestData()
    {
        try
        {
            String sql ="SELECT * FROM airports";

            Cursor mCur = mDb.rawQuery(sql, null);
            if (mCur!=null)
            {
                mCur.moveToNext();
            }
            return mCur;
        }
        catch (SQLException mSQLException)
        {
            Log.e(TAG, "getTestData >>"+ mSQLException.toString());
            throw mSQLException;
        }
    }

    public List<IataModel> getAllIata() {
        List<IataModel> notes = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM airports";


        Cursor cursor = mDb.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                IataModel note = new IataModel();
                note.setIata(cursor.getString(cursor.getColumnIndex("iata_code")));
                note.setLattitude(cursor.getDouble(cursor.getColumnIndex("lat_decimal")));
                note.setLongitude(cursor.getDouble(cursor.getColumnIndex("lon_decimal")));

                notes.add(note);
            } while (cursor.moveToNext());
        }

        // close db connection


        // return notes list
        return notes;
    }

     public IataModel getsavedIataDetails(String name) {


        Cursor cursor = mDb.query("airports", new String[] {"iata_code","name","city","country", "lat_decimal","lon_decimal" }, "iata_code" + "=?",
                new String[] { name }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

         IataModel iataModel = new IataModel();
         iataModel.setIata(cursor.getString(cursor.getColumnIndex("iata_code")));
         iataModel.setName(cursor.getString(cursor.getColumnIndex("name")));
         iataModel.setCity(cursor.getString(cursor.getColumnIndex("city")));
         iataModel.setCountry(cursor.getString(cursor.getColumnIndex("country")));
         iataModel.setLattitude(cursor.getDouble(cursor.getColumnIndex("lat_decimal")));
         iataModel.setLongitude(cursor.getDouble(cursor.getColumnIndex("lon_decimal")));

        return iataModel;
    }


    public void updateLatLong(String iata,double lat,double lng) {

        ContentValues cv = new ContentValues();
        cv.put("lat_decimal",lat);
        cv.put("lon_decimal",lng);

        mDb.update("airports", cv, "iata_code = ?", new String[]{iata});
    }
}
