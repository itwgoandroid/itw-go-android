package com.j2ptechnologies.itwtravelgo.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import com.j2ptechnologies.itwtravelgo.model.DBSaveModel;
import com.j2ptechnologies.itwtravelgo.model.NotificationDetailsModel;

import java.util.ArrayList;
import java.util.List;


public class DatabaseHandler extends SQLiteOpenHelper {


	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = "itwTravelAndGoDB";
	private static final String TABLE_INFORMATION = "offlineTable";
	private static final String NOTIFICATION_TABLE = "notifiaction";
	private static final String KEY_NAME = "name";
	private static final String KEY_VALUE = "value";

	public DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_INFORMATION + "(" + KEY_NAME + " TEXT," + KEY_VALUE + " TEXT" + ")";
		db.execSQL(CREATE_CONTACTS_TABLE);

		String NOTIFICATION_TABLE_CREATE = "CREATE TABLE " + NOTIFICATION_TABLE + " (NOTI_ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
				"TITLE TEXT, MESSAGE TEXT, IMG_URL TEXT )";

		db.execSQL(NOTIFICATION_TABLE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_INFORMATION);
		db.execSQL("DROP TABLE IF EXISTS " + NOTIFICATION_TABLE);
		onCreate(db);
	}

	public void addDetails(String name, String value) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_NAME, name);
		values.put(KEY_VALUE, value);

		db.insert(TABLE_INFORMATION, null, values);
		db.close();
	}



	public DBSaveModel getsavedValue(String name) {
		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(TABLE_INFORMATION, new String[]{KEY_NAME, KEY_VALUE}, KEY_NAME + "=?",
				new String[]{name}, null, null, null, null);
		if (cursor != null)
			cursor.moveToFirst();

		DBSaveModel value = new DBSaveModel(cursor.getString(0), cursor.getString(1));
		return value;
	}

	public List<NotificationDetailsModel> getAllNotification() {
		List<NotificationDetailsModel> notificationList = new ArrayList<NotificationDetailsModel>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + NOTIFICATION_TABLE;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				NotificationDetailsModel notificationDetailsModel = new NotificationDetailsModel();
				notificationDetailsModel.setId(Integer.parseInt(cursor.getString(0)));
				notificationDetailsModel.setTitle(cursor.getString(1));
				notificationDetailsModel.setMessage(cursor.getString(2));
				notificationDetailsModel.setImageUrl(cursor.getString(3));
				// Adding contact to list
				notificationList.add(notificationDetailsModel);
			} while (cursor.moveToNext());
		}

		// return contact list
		return notificationList;
	}

	public boolean isDataExist(String name) {
		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(TABLE_INFORMATION, new String[]{KEY_NAME, KEY_VALUE}, KEY_NAME + "=?",
				new String[]{name}, null, null, null, null);
		if (cursor != null) {
			if (cursor.getCount() == 0) {
				return false;
			} else {
				return true;
			}
		}
		return false;
	}


	public int updateDetails(DBSaveModel saveDetails) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_NAME, saveDetails.getName());
		values.put(KEY_VALUE, saveDetails.getValue());
		return db.update(TABLE_INFORMATION, values, KEY_NAME + " = ?",
				new String[]{saveDetails.getName()});
	}

	public void deleteDetails(String name) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_INFORMATION, KEY_NAME + " = ?",
				new String[]{name});
		db.close();
	}

	public void deleteNotification(int id) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("DELETE FROM " + NOTIFICATION_TABLE + " WHERE NOTI_ID "  + " = " + id + "");

		db.close();
	}

	public boolean addNotifications(String title, String messages, String imgUrl) {
		boolean isInsrtd = false;

		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put("TITLE", title);
		values.put("MESSAGE", messages);
		values.put("IMG_URL", imgUrl);

		long i = db.insert(NOTIFICATION_TABLE, null, values);
		if (i <= -1)
			isInsrtd = false;
		else
			isInsrtd = true;

		db.close();

		return isInsrtd;
	}



	public int getNotificationCount() {
		String countQuery = "SELECT  * FROM " + NOTIFICATION_TABLE;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		int count=cursor.getCount();
		cursor.close();

		// return count
		return count;
	}


}
