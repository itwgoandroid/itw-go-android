package com.j2ptechnologies.itwtravelgo.view;

import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;
import com.j2ptechnologies.itwtravelgo.Activity.Dashboard;
import com.j2ptechnologies.itwtravelgo.Activity.RegisterActivity;
import com.j2ptechnologies.itwtravelgo.Activity.ResetPassword;
import com.j2ptechnologies.itwtravelgo.R;
import com.j2ptechnologies.itwtravelgo.Utils.CatLoadingView;
import com.j2ptechnologies.itwtravelgo.Utils.CommonMethods;
import com.j2ptechnologies.itwtravelgo.Utils.CommonMethodsFragments;
import com.j2ptechnologies.itwtravelgo.Utils.Constants;
import com.j2ptechnologies.itwtravelgo.Utils.LovelyInfoDialog;
import com.j2ptechnologies.itwtravelgo.Utils.StaticConfig;
import com.j2ptechnologies.itwtravelgo.api.GetApi;
import com.j2ptechnologies.itwtravelgo.api.PostApi;
import com.j2ptechnologies.itwtravelgo.app.App;
import com.j2ptechnologies.itwtravelgo.db.DatabaseHandler;
import com.j2ptechnologies.itwtravelgo.model.EventBusModel;
import com.j2ptechnologies.itwtravelgo.util.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.greenrobot.event.EventBus;

import static com.j2ptechnologies.itwtravelgo.Utils.CommonMethodsFragments.getDetails;
import static com.j2ptechnologies.itwtravelgo.util.Util.displayName;


public class LoginActivity extends CommonMethods implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {

    //Constants
    private static final String TAG = LoginActivity.class.getSimpleName();
    private static final int RC_SIGN_IN = 9001;
    private boolean firstTimeAccess;
    CatLoadingView mView;
    private SignInButton mSignInButton;

    //Firebase and GoogleApiClient
    private GoogleApiClient mGoogleApiClient;
    private FirebaseAuth mFirebaseAuth;
    FloatingActionButton fab;
    private final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
    private EditText editTextUsername, editTextPassword;
    TextView resetPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mView = new CatLoadingView();
       /* if (!Util.verificaConexao(this)){
            Util.initToast(this,"Você não tem conexão com internet");
            finish();
        }*/

        DatabaseHandler db = new DatabaseHandler(getBaseContext());
        if (CommonMethodsFragments.getDetails("isRegistered", db).equals("true")) {
            Intent i = new Intent(getBaseContext(), Dashboard.class);
            i.putExtra("from", "login");
            startActivity(i);
            finish();
        }


        mSignInButton = (SignInButton) findViewById(R.id.sign_in_button);
        resetPassword = (TextView) findViewById(R.id.resetPassword);
        mSignInButton.setOnClickListener(this);
        resetPassword.setOnClickListener(this);


        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        // Initialize FirebaseAuth
        mFirebaseAuth = FirebaseAuth.getInstance();

        getSupportActionBar().hide();

        fab = (FloatingActionButton) findViewById(R.id.fab);
        editTextUsername = (EditText) findViewById(R.id.et_username);
        editTextPassword = (EditText) findViewById(R.id.et_password);

        Button bt_go = (Button) findViewById(R.id.bt_go);
        bt_go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!validateEmail(editTextUsername.getText().toString().trim())) {
                    editTextUsername.setError("Invalid Email");
                    return;
                } else if (editTextPassword.getText().toString() != null && editTextPassword.getText().toString().trim().length() < 6) {
                    editTextPassword.setError("Password length must be min 6");
                    return;
                } else {
                    JSONObject json = new JSONObject();
                    try {
                        json.put("email", editTextUsername.getText().toString().trim());
                        json.put("password", editTextPassword.getText().toString().trim());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    login(json, editTextUsername.getText().toString().trim(), editTextPassword.getText().toString().trim());

                }
                /*Intent i=new Intent(getBaseContext(),Dashboard.class);
                startActivity(i);*/
            }
        });
        firstTimeAccess = true;

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                GoogleSignInAccount account = result.getSignInAccount();
                firebaseAuthWithGoogle(account);
            } else {
                Log.e(TAG, "Google Sign In failed.");
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_in_button:
                signIn();
                break;

                case R.id.resetPassword:
                    if (!validateEmail(editTextUsername.getText().toString().trim())) {
                        editTextUsername.setError("Invalid Email");
                        return;
                    }
                resetMyPassword(editTextUsername.getText().toString().trim());
                break;
            default:
                return;
        }
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
        Util.initToast(this, "Google Play Services error.");
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void firebaseAuthWithGoogle(final GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGooogle:" + acct.getId());
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mFirebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInWithCredential", task.getException());
                            Util.initToast(LoginActivity.this, "Authentication failed");
                        } else {

                            displayName = acct.getDisplayName();
                            Util.email = acct.getEmail();
                            //storetoFirebase(acct.getDisplayName(), acct.getEmail());

                        }
                    }
                });
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @SuppressLint("RestrictedApi")
    public void clickRegisterLayout(View view) {
        getWindow().setExitTransition(null);
        getWindow().setEnterTransition(null);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ActivityOptions options =
                    ActivityOptions.makeSceneTransitionAnimation(this, fab, fab.getTransitionName());
            startActivityForResult(new Intent(this, RegisterActivity.class), StaticConfig.REQUEST_CODE_REGISTER, options.toBundle());
        } else {
            startActivityForResult(new Intent(this, RegisterActivity.class), StaticConfig.REQUEST_CODE_REGISTER);

        }
    }


    public void clickLogin(View view) {
        String username = editTextUsername.getText().toString();
        String password = editTextPassword.getText().toString();

        //Intent i=new Intent(getBaseContext(),Dashboard.class);
        // startActivity(i);
        /*if (validate(username, password)) {

        } else {
            Toast.makeText(this, "Invalid email or empty password", Toast.LENGTH_SHORT).show();
        }*/
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult(RESULT_CANCELED, null);
        finish();
    }

    private boolean validate(String emailStr, String password) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
        return (password.length() > 0 || password.equals(";")) && matcher.find();
    }

    public void clickResetPassword(View view) {
        String username = editTextUsername.getText().toString();
        if (validate(username, ";")) {

        } else {
            Toast.makeText(this, "Invalid email", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Dinh nghia cac ham tien ich cho quas trinhf dang nhap, dang ky,...
     */
    class AuthUtils {
        /**
         * Action register
         *
         * @param email
         * @param password
         */

    }

    public void showDialog() {
        mView.setClickCancelAble(false);
        mView.show(getSupportFragmentManager(), "");
    }

    public void dismissDialog() {
        mView.dismiss();
    }

    public void login(JSONObject json, final String email, final String password) {
        String URL = Constants.loginUrl;
        showDialog();

        PostApi api = new PostApi(Request.Method.POST, URL, json, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                dismissDialog();
                try {
                    int code = jsonObject.getInt("code");
                    if (code == 200) {
                        DatabaseHandler db = new DatabaseHandler(getBaseContext());
                        CommonMethodsFragments.addDetails("currentEmail", email, db);
                        CommonMethodsFragments.addDetails("myCurrentPassword", password, db);
                        CommonMethodsFragments.addDetails("isRegistered", "true", db);
                        Intent i = new Intent(getBaseContext(), Dashboard.class);
                        i.putExtra("from", "login");
                        startActivity(i);
                        finish();
                    } else {
                        Snackbar snackbar = null;
                        dismissDialog();
                        final Snackbar finalSnackbar = snackbar;
                        snackbar = Snackbar
                                .make(getWindow().getDecorView(), "Invalid Username or Password", Snackbar.LENGTH_LONG)
                                .setAction(" ", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {

                                    }
                                });

                        snackbar.show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Snackbar snackbar = null;
                dismissDialog();
                final Snackbar finalSnackbar = snackbar;
                snackbar = Snackbar
                        .make(getWindow().getDecorView(), "Something went wrong", Snackbar.LENGTH_LONG)
                        .setAction("", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                            }
                        });

                snackbar.show();


            }
        });

        App.getVolleyQueue().add(api);
    }


    public void resetMyPassword(final String email) {
        String URL = Constants.resetPassword+email;
        showDialog();
        GetApi updateProfileApi = new GetApi(
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {

                        dismissDialog();

                        try {
                            int code=jsonObject.getInt("code");

                            if(code==200)
                            {
                                Intent i=new Intent(getBaseContext(), ResetPassword.class);
                                i.putExtra("currentEmail",email);
                                startActivity(i);
                            }else
                            {
                                new LovelyInfoDialog(getBaseContext())
                                        .setTopColorRes(R.color.colorAccent)
                                        .setIcon(R.drawable.ic_tab_infor)

                                        //This will add Don't show again checkbox to the dialog. You can pass any ID as argument
                                        .setTitleGravity(Gravity.CENTER)
                                        .setNotShowAgainOptionChecked(false)
                                        .setTitle("Error")

                                        .setMessage("Invalid Email. Please check your email")
                                        .show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                    dismissDialog();
            }
        }, URL);

        App.getVolleyQueue().add(updateProfileApi);

    }

}
