package com.j2ptechnologies.itwtravelgo.notifications;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.j2ptechnologies.itwtravelgo.Activity.ChatActivity;
import com.j2ptechnologies.itwtravelgo.Activity.Dashboard;
import com.j2ptechnologies.itwtravelgo.Activity.NotificationActivity;
import com.j2ptechnologies.itwtravelgo.R;
import com.j2ptechnologies.itwtravelgo.Utils.CommonMethodsFragments;
import com.j2ptechnologies.itwtravelgo.db.DatabaseHandler;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;


/**
 * Created by L on 09/05/2017.
 * Copyright (c) 2017 Centroida. All rights reserved.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String NOTIFICATION_ID_EXTRA = "notificationId";
    private static final String IMAGE_URL_EXTRA = "imageUrl";
    private static final String ADMIN_CHANNEL_ID = "admin_channel";
    private NotificationManager notificationManager;


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Intent notificationIntent = new Intent(this, Dashboard.class);
        notificationIntent.putExtra("from", "notification");
        if (NotificationActivity.isAppRunning) {
            //
        } else {
            //Show notification as usual
        }

        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        final PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0 /* Request code */, notificationIntent,
                PendingIntent.FLAG_ONE_SHOT);

        //You should use an actual ID instead
        int notificationId = new Random().nextInt(60000);


        Bitmap bitmap = getBitmapfromUrl(remoteMessage.getData().get("image-url"));

        Intent likeIntent = new Intent(this, LikeService.class);
        likeIntent.putExtra(NOTIFICATION_ID_EXTRA, notificationId);
        likeIntent.putExtra(IMAGE_URL_EXTRA, remoteMessage.getData().get("image-url"));
        PendingIntent likePendingIntent = PendingIntent.getService(this,
                notificationId + 1, likeIntent, PendingIntent.FLAG_ONE_SHOT);


        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            setupChannels();
        }



        if(remoteMessage.getData().get("message")!=null) {
            DatabaseHandler db = new DatabaseHandler(getBaseContext());
            String message = "";

            int messCount = 0;
            if (CommonMethodsFragments.getDetails("notificationCount",db) == null || CommonMethodsFragments.getDetails("notificationCount",db).equals("")) {
                messCount = 1;

            } else {
                String mess = CommonMethodsFragments.getDetails("notificationCount",db);
                messCount = Integer.parseInt(mess) + 1;
            }

            CommonMethodsFragments.addDetails("notificationCount", messCount + "",db);

            NotificationCompat.Builder notificationBuilder =
                    new NotificationCompat.Builder(this, ADMIN_CHANNEL_ID)
                            .setLargeIcon(bitmap)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setContentTitle(remoteMessage.getData().get("title"))
                            .setStyle(new NotificationCompat.BigPictureStyle()
                                    .setSummaryText(remoteMessage.getData().get("message"))
                                    .bigPicture(bitmap))/*Notification with Image*/
                            .setContentText(remoteMessage.getData().get("message"))
                            .setAutoCancel(true)
                            .setSound(defaultSoundUri)
                            .addAction(R.drawable.ic_favorite_true,
                                    getString(R.string.notification_add_to_cart_button), likePendingIntent)
                            .setContentIntent(pendingIntent);

            CommonMethodsFragments commonMethodsFragments = new CommonMethodsFragments();
            commonMethodsFragments.addDetailsForNotification(remoteMessage.getData().get("title"), remoteMessage.getData().get("message"), remoteMessage.getData().get("image-url"), new DatabaseHandler(getBaseContext()));
            notificationManager.notify(notificationId, notificationBuilder.build());
        }else {

            DatabaseHandler db = new DatabaseHandler(getBaseContext());
            String message = "";

            int messCount = 0;
            if (CommonMethodsFragments.getDetails("messageCount",db) == null || CommonMethodsFragments.getDetails("messageCount",db).equals("")) {
                messCount = 1;

            } else {
                String mess = CommonMethodsFragments.getDetails("messageCount",db);
                messCount = Integer.parseInt(mess) + 1;
            }

            CommonMethodsFragments.addDetails("messageCount", messCount + "",db);
            switch (messCount) {
                case 1:
                    message = "You have 1 important message from Admin";
                    break;

                case 2:
                    message = "You have 2 very important messages from Admin";
                    break;
                case 3:
                    message = "You have 3 messages from Admin. Please Reply";
                    break;
                case 4:
                    message = "You have 4 messages from Admin.Please chat with our Admin";
                    break;
                case 5:
                    message = "You have not replied to us. Please go to chat section and chat with us";
                    break;
                default:
                    message = "You have " + CommonMethodsFragments.getDetails("messageCount",db) + " messages from Admin. Please Reply";

                    break;
            }


            if (!ChatActivity.isAppRunning) {
                Intent notificationIntent2 = new Intent(this, ChatActivity.class);

                notificationIntent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                final PendingIntent pendingIntent2 = PendingIntent.getActivity(this,
                        0 /* Request code */, notificationIntent2,
                        PendingIntent.FLAG_ONE_SHOT);
                NotificationCompat.Builder notificationBuilder =
                        new NotificationCompat.Builder(this, ADMIN_CHANNEL_ID)
                                .setLargeIcon(bitmap)
                                .setSmallIcon(R.mipmap.ic_launcher)
                                .setContentTitle("ITW Go")
                                .setContentText(message)
                                .setAutoCancel(true)
                                .setSound(defaultSoundUri)
                                .setContentIntent(pendingIntent2);

                // CommonMethodsFragments commonMethodsFragments = new CommonMethodsFragments();
                // commonMethodsFragments.addDetailsForNotification(remoteMessage.getData().get("title"), remoteMessage.getData().get("message"), remoteMessage.getData().get("image-url"), new DatabaseHandler(getBaseContext()));
                notificationManager.notify(notificationId, notificationBuilder.build());
            }
        }
    }

    public Bitmap getBitmapfromUrl(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            return BitmapFactory.decodeStream(input);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void setupChannels() {
        CharSequence adminChannelName = getString(R.string.notifications_admin_channel_name);
        String adminChannelDescription = getString(R.string.notifications_admin_channel_description);

        NotificationChannel adminChannel;
        adminChannel = new NotificationChannel(ADMIN_CHANNEL_ID, adminChannelName, NotificationManager.IMPORTANCE_LOW);
        adminChannel.setDescription(adminChannelDescription);
        adminChannel.enableLights(true);
        adminChannel.setLightColor(Color.RED);
        adminChannel.enableVibration(true);
        if (notificationManager != null) {
            notificationManager.createNotificationChannel(adminChannel);
        }
    }
}