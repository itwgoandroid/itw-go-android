package com.j2ptechnologies.itwtravelgo.notifications;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.j2ptechnologies.itwtravelgo.Utils.CommonMethodsFragments;
import com.j2ptechnologies.itwtravelgo.db.DatabaseHandler;
import com.j2ptechnologies.itwtravelgo.util.Util;

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.e(TAG, "Refreshed token: " + refreshedToken);
        DatabaseHandler db=new DatabaseHandler(getBaseContext());
        CommonMethodsFragments.addDetails("refreshedToken",refreshedToken,db);


        SharedPreferences preferences =
                PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        preferences.edit().putString(Util.FIREBASE_TOKEN, refreshedToken).apply();
    }





}