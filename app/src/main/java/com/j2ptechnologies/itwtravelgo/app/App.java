package com.j2ptechnologies.itwtravelgo.app;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.os.StrictMode;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.firebase.client.Firebase;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.FirebaseDatabase;


/**
 * Created by vijay on 27/5/15.
 */
public class App extends Application {

    private static RequestQueue volleyQueue;
    private static Context appContext;



    public static int version =0;


    @Override
    public void onCreate() {

        super.onCreate();
        appContext = this;
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(
                    getPackageName(), 0);
            version = pInfo.versionCode;
        } catch (Exception e) {
            version=0;
        }

        Firebase.setAndroidContext(this);

        //Newer version of Firebase
        if (!FirebaseApp.getApps(this).isEmpty()) {
            FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        }

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        volleyQueue = Volley.newRequestQueue(getApplicationContext());


    }





    public static RequestQueue getVolleyQueue() {
        if (volleyQueue == null) {
            volleyQueue = Volley.newRequestQueue(getAppContext());
        }
        return volleyQueue;
    }

    public static Context getAppContext() {
        return appContext;
    }







}