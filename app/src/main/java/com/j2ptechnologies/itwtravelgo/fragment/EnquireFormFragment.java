package com.j2ptechnologies.itwtravelgo.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.html.WebColors;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import com.j2ptechnologies.itwtravelgo.R;
import com.j2ptechnologies.itwtravelgo.Utils.CommonMethodsFragments;
import com.j2ptechnologies.itwtravelgo.db.DatabaseHandler;
import com.j2ptechnologies.itwtravelgo.sendmail.GMailSender;
import com.j2ptechnologies.itwtravelgo.util.Util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link EnquireFormFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link EnquireFormFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EnquireFormFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private EditText edt_destination, edt_departurecity, edt_email, edt_phonenumber;
    private Button btnfixed, btnflexible, btnanytime, btnsubmit;
    private OnFragmentInteractionListener mListener;

    private int slctdTime = 0;
    private File filetenant;

    public EnquireFormFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment EnquireFormFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static EnquireFormFragment newInstance(String param1, String param2) {
        EnquireFormFragment fragment = new EnquireFormFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.activity_form_layout, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE | WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        viewInitialization(view);

        btnsubmit.setOnClickListener(this);
        btnfixed.setOnClickListener(this);
        btnflexible.setOnClickListener(this);
        btnanytime.setOnClickListener(this);

        DatabaseHandler db = new DatabaseHandler(getActivity());
        edt_email.setText(CommonMethodsFragments.getDetails("currentEmail", db));
        return view;

    }

    private void viewInitialization(View view) {
        edt_destination = view.findViewById(R.id.edt_destination);
        edt_departurecity = view.findViewById(R.id.edt_departurecity);
        edt_email = view.findViewById(R.id.edt_email);
        edt_phonenumber = view.findViewById(R.id.edt_phonenumber);
        btnsubmit = view.findViewById(R.id.btnsubmit);

        btnflexible = view.findViewById(R.id.btnflexible);
        btnfixed = view.findViewById(R.id.btnfixed);
        btnanytime = view.findViewById(R.id.btnanytime);

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnsubmit:

                sendmail();
                break;
            case R.id.btnfixed:
                fixedselected();
                break;
            case R.id.btnflexible:
                flexibleselected();
                break;
            case R.id.btnanytime:
                anytimeSelected();
                break;
        }
    }

    private void anytimeSelected() {
        slctdTime = 3;
        btnanytime.setBackgroundResource(R.drawable.button_with_solid_color_onrelease_orange);
        btnfixed.setBackgroundResource(R.drawable.button_with_solid_color_onrelease_purple);
        btnflexible.setBackgroundResource(R.drawable.button_with_solid_color_onrelease_purple);
    }

    private void flexibleselected() {
        slctdTime = 2;
        btnflexible.setBackgroundResource(R.drawable.button_with_solid_color_onrelease_orange);
        btnfixed.setBackgroundResource(R.drawable.button_with_solid_color_onrelease_purple);
        btnanytime.setBackgroundResource(R.drawable.button_with_solid_color_onrelease_purple);
    }

    private void fixedselected() {
        slctdTime = 1;
        btnfixed.setBackgroundResource(R.drawable.button_with_solid_color_onrelease_orange);
        btnflexible.setBackgroundResource(R.drawable.button_with_solid_color_onrelease_purple);
        btnanytime.setBackgroundResource(R.drawable.button_with_solid_color_onrelease_purple);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void sendmail() {

        String usermail = edt_email.getText().toString().trim();
        String usermobile = edt_phonenumber.getText().toString().trim();
        String strdestination = edt_destination.getText().toString().trim();
        String strdepsrturecity = edt_departurecity.getText().toString().trim();
        String strdate = null;
        if (slctdTime == 0) {
            strdate = "Not Selected";
        } else if (slctdTime == 1) {
            strdate = "Fixed Date";
        } else if (slctdTime == 2) {
            strdate = "Flexible date";
        } else if (slctdTime == 3) {
            strdate = "Any Time";
        }

        filetenant = new File(Environment.getExternalStorageDirectory()
                .getAbsolutePath() + "/ITW");

        if (!filetenant.exists()) {
            filetenant.mkdir();
        }

        File flepath = new File(filetenant.getAbsolutePath() + "/" + "ITWENQUIRE_01" + ".pdf");
        String filedr = flepath.getAbsolutePath();

        if (usermail.equals("")) {
            Toast.makeText(getActivity(), "Please enter correct Email", Toast.LENGTH_SHORT).show();
        } else if (strdestination.equals("")) {
            Toast.makeText(getActivity(), "Please enter correct Destination", Toast.LENGTH_SHORT).show();
        } else if (strdepsrturecity.equals("")) {
            Toast.makeText(getActivity(), "Please enter correct Departure City", Toast.LENGTH_SHORT).show();
        } else if ((usermobile.length() < 10) || usermobile.equals("")) {
            Toast.makeText(getActivity(), "Please enter correct mobile number...", Toast.LENGTH_SHORT).show();
        } else {
            try {
                createPdf(filedr, strdepsrturecity, strdestination, strdate, usermail, usermobile);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (DocumentException e) {
                e.printStackTrace();
            }
        }
       /* new SendMailTask().execute(usermail, usermobile, strdestination, strdepsrturecity);*/
    }

    private class SendMailTask extends AsyncTask<String, String, String> {

        ProgressDialog maildialog;
        String strusermail, strusermobile, strdestination, strdepsrturecity, strdate;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            maildialog = new ProgressDialog(getActivity());
            maildialog.setMessage("Sending Enquire...");
            maildialog.setCancelable(false);
            maildialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            strusermail = params[0];
            strusermobile = params[1];
            strdestination = params[2];
            strdepsrturecity = params[3];

            try {
                String messageTouser = "Thank you for showing intrest." + "\n" + "Our experts agents will get back to you with quotes in 24-48 hours." + "\n";

                //1st parameter Mail ID, 2nd parameter email password
                GMailSender sender = new GMailSender("itwtravelgo@gmail.com", "itwtravelgo@123");
                // sender.sendMail(params[0], params[1], "shrikantkorigeri@gmail.com", "skshreek2@gmail.com");
                sender.addAttachment(Environment.getExternalStorageDirectory().getAbsolutePath() + "/ITW/" + "ITWENQUIRE_01" + ".pdf", "ITWENQUIRE_01" + ".pdf");
                sender.sendMail("Enquire", messageTouser, "itwtravelgo@gmail.com", strusermail + ",shrikantkorigeri@gmail.com");

            } catch (Exception e) {
                Log.e("SendMail", e.getMessage(), e);
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {

            maildialog.dismiss();
            Toast.makeText(getActivity(), "Mail sent....", Toast.LENGTH_SHORT).show();

        }
    }

    private void createPdf(String filedir, String departure, String destination, String slctddate, String stremail, String strmobilenumber) throws IOException, DocumentException {
        Document document = new Document();
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(filedir));

        MyFooter event = new MyFooter();
        writer.setPageEvent(event);
        document.open();

        Paragraph p = null, p1, p2, p4;

        Image imagelogo = null;
        try {
            Bitmap bmp = Util.getCompanyLogo(getActivity());

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 20, stream);
            imagelogo = Image.getInstance(stream.toByteArray());
            // document.add(image);
        } catch (IOException ex) {
            return;
        }

        BaseColor myColor = WebColors.getRGBColor("#373747");
        Font fontcomapny = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD, BaseColor.WHITE);
        float[] columnWidthsimage = {1, 5};
        PdfPTable tableimagehead = new PdfPTable(columnWidthsimage);
        tableimagehead.setSpacingBefore(20f);
        tableimagehead.setSpacingAfter(20f);
        tableimagehead.setWidthPercentage(100);

        PdfPCell imagecell = new PdfPCell();
        imagelogo.setWidthPercentage(50);
        imagecell.setBorder(Rectangle.NO_BORDER);
        imagecell.addElement(imagelogo);
        imagecell.setBackgroundColor(myColor);
        imagecell.setHorizontalAlignment(Element.ALIGN_CENTER);
        imagecell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        tableimagehead.addCell(imagecell);

        PdfPCell cellcmpnyname = new PdfPCell(new Phrase(Util.cmpname, fontcomapny));
        cellcmpnyname.setBorder(Rectangle.NO_BORDER);
        cellcmpnyname.setBackgroundColor(myColor);
        cellcmpnyname.setHorizontalAlignment(Element.ALIGN_LEFT);
        cellcmpnyname.setVerticalAlignment(Element.ALIGN_MIDDLE);
        tableimagehead.addCell(cellcmpnyname);
        tableimagehead.setHorizontalAlignment(Element.ALIGN_CENTER);

        document.add(tableimagehead);

        try {
            p = new Paragraph(Util.convertDate(new Date(), "MMM dd, yyyy"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        p.setAlignment(Element.ALIGN_RIGHT);
        document.add(p);

        PdfPTable tablen = new PdfPTable(2);
        tablen.setSpacingBefore(5f);
        tablen.setSpacingAfter(2f);
        tablen.setWidthPercentage(100);

        float[] columnWidths = {1, 6, 3};
        PdfPTable table2 = new PdfPTable(columnWidths);
        table2.setHorizontalAlignment(Element.ALIGN_CENTER);
        table2.setSpacingBefore(20f);
        table2.setSpacingAfter(20f);

        Font font = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.BLACK);

        PdfPCell c1 = new PdfPCell(new Phrase("SL.NO.", font));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
        c1.setFixedHeight(35f);
        table2.addCell(c1);

        c1 = new PdfPCell(new Phrase("DESCRIPTION", font));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
        table2.addCell(c1);

        c1 = new PdfPCell(new Phrase("INTREST", font));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
        table2.addCell(c1);
        table2.setHeaderRows(1);

        Font rowfont = new Font(Font.FontFamily.HELVETICA, 9, Font.NORMAL, BaseColor.BLACK);

        //First Row
        PdfPCell frstCell = new PdfPCell(new Phrase("01", rowfont));
        frstCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        frstCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        frstCell.setFixedHeight(25f);
        table2.addCell(frstCell);

        frstCell = new PdfPCell(new Phrase("Departure City", rowfont));
        frstCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        frstCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        table2.addCell(frstCell);

        frstCell = new PdfPCell(new Phrase(String.format(departure), rowfont));
        frstCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        frstCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        table2.addCell(frstCell);

        //Second Row
        PdfPCell scndCell = new PdfPCell(new Phrase("02", rowfont));
        scndCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        scndCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        scndCell.setFixedHeight(25f);
        table2.addCell(scndCell);

        scndCell = new PdfPCell(new Phrase("Destination ", rowfont));
        scndCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        scndCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        table2.addCell(scndCell);

        scndCell = new PdfPCell(new Phrase(destination, rowfont));
        scndCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        frstCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        table2.addCell(scndCell);

        //Third Row
        PdfPCell frthCell = new PdfPCell(new Phrase("03", rowfont));
        frthCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        frthCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        frthCell.setFixedHeight(25f);
        table2.addCell(frthCell);

        frthCell = new PdfPCell(new Phrase("Selected Date ", rowfont));
        frthCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        frthCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        table2.addCell(frthCell);

        frthCell = new PdfPCell(new Phrase(slctddate, rowfont));
        frthCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        frthCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        table2.addCell(frthCell);

        //Fourth row
        PdfPCell fifthCell = new PdfPCell(new Phrase("04", rowfont));
        fifthCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        fifthCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        fifthCell.setFixedHeight(25f);
        table2.addCell(fifthCell);

        fifthCell = new PdfPCell(new Phrase("Customer Email ", rowfont));
        fifthCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        fifthCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        table2.addCell(fifthCell);

        fifthCell = new PdfPCell(new Phrase(stremail, rowfont));
        fifthCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        fifthCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        table2.addCell(fifthCell);


        //Fifth row
        PdfPCell sixthcell = new PdfPCell(new Phrase("05", rowfont));
        sixthcell.setHorizontalAlignment(Element.ALIGN_CENTER);
        sixthcell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        sixthcell.setFixedHeight(25f);
        table2.addCell(sixthcell);

        sixthcell = new PdfPCell(new Phrase("Customer Mobile ", rowfont));
        sixthcell.setHorizontalAlignment(Element.ALIGN_CENTER);
        sixthcell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        table2.addCell(sixthcell);

        sixthcell = new PdfPCell(new Phrase(strmobilenumber, rowfont));
        sixthcell.setHorizontalAlignment(Element.ALIGN_CENTER);
        sixthcell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        table2.addCell(fifthCell);


        document.add(table2);
        // document.add(table);

        Font ffont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL);

        String ws = "For, " + "\n" + Util.cmpname;
        p1 = new Paragraph(String.format("%s", ws), ffont);
        p1.setIndentationLeft(5);
        p1.setIndentationRight(50);
        p1.setSpacingBefore(20f);
        p1.setSpacingAfter(10f);


        document.add(p1);
        document.close();

        /*SendMailTask sendMailTask = new SendMailTask();
        sendMailTask.execute(new String[]{"Rent Statement for the Month of " + strdate, String.valueOf(intrpt), String.valueOf(maxrdid)});*/

        new SendMailTask().execute(stremail, strmobilenumber, destination, departure);

    }

    public class MyFooter extends PdfPageEventHelper {

        Font ffont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL);

        public void onEndPage(PdfWriter writer, Document document) {
            PdfContentByte cb = writer.getDirectContent();
            Phrase header = new Phrase("Acknowledgement", ffont);
            Phrase footer = new Phrase(Util.cmpname, ffont);
            ColumnText.showTextAligned(cb, Element.ALIGN_CENTER, header,
                    (document.right() - document.left()) / 2 + document.leftMargin(), document.top() + 10, 0);
            ColumnText.showTextAligned(cb, Element.ALIGN_CENTER, footer,
                    (document.right() - document.left()) / 2 + document.leftMargin(), document.bottom() - 10, 0);
        }
    }
}
