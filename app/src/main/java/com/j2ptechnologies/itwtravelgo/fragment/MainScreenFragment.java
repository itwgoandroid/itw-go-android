package com.j2ptechnologies.itwtravelgo.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.j2ptechnologies.itwtravelgo.Activity.AboutActivity;
import com.j2ptechnologies.itwtravelgo.Activity.ChatActivity;
import com.j2ptechnologies.itwtravelgo.Activity.Dashboard;
import com.j2ptechnologies.itwtravelgo.R;
import com.j2ptechnologies.itwtravelgo.Utils.CommonMethodsFragments;
import com.j2ptechnologies.itwtravelgo.Utils.Constants;
import com.j2ptechnologies.itwtravelgo.Utils.LovelyInfoDialog;
import com.j2ptechnologies.itwtravelgo.Utils.MyService;
import com.j2ptechnologies.itwtravelgo.api.GetApi;
import com.j2ptechnologies.itwtravelgo.api.PostApi;
import com.j2ptechnologies.itwtravelgo.app.App;
import com.j2ptechnologies.itwtravelgo.db.DatabaseHandler;
import com.j2ptechnologies.itwtravelgo.model.DashboardTripsListModel;
import com.j2ptechnologies.itwtravelgo.model.EventBusModel;
import com.j2ptechnologies.itwtravelgo.transition.MainActivity;
import com.j2ptechnologies.itwtravelgo.util.PrefUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

import de.greenrobot.event.EventBus;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MainScreenFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MainScreenFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MainScreenFragment extends CommonMethodsFragments {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    RelativeLayout explore;
    RelativeLayout aboutUs;
    RelativeLayout trips;
    RelativeLayout quotaion;
    RelativeLayout notification;
    RelativeLayout chat;
    String adminChatCount;
    String notificationMessageCount;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    public com.j2ptechnologies.itwtravelgo.Utils.CircularTextView chatCount,notificationCount,tripCount,quotationCountNew;
    private OnFragmentInteractionListener mListener;

    public MainScreenFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MainScreenFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MainScreenFragment newInstance(String param1, String param2) {
        MainScreenFragment fragment = new MainScreenFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main_screen, container, false);
        explore = (RelativeLayout) view.findViewById(R.id.explore);
        aboutUs = (RelativeLayout) view.findViewById(R.id.aboutUs);
        trips = (RelativeLayout) view.findViewById(R.id.trips);
        quotaion = (RelativeLayout) view.findViewById(R.id.quotation);
        notification = (RelativeLayout) view.findViewById(R.id.notification);
        chat = (RelativeLayout) view.findViewById(R.id.chat);
        chatCount = (com.j2ptechnologies.itwtravelgo.Utils.CircularTextView) view.findViewById(R.id.chatCount);
        notificationCount = (com.j2ptechnologies.itwtravelgo.Utils.CircularTextView) view.findViewById(R.id.notificationCount);
        tripCount = (com.j2ptechnologies.itwtravelgo.Utils.CircularTextView) view.findViewById(R.id.tripCount);
        quotationCountNew = (com.j2ptechnologies.itwtravelgo.Utils.CircularTextView) view.findViewById(R.id.quotationCountNew);

        DatabaseHandler db = new DatabaseHandler(getActivity());
        getTrips(db);
        explore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), MainActivity.class);
                startActivity(i);
            }
        });
        aboutUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), AboutActivity.class);
                startActivity(i);
            }
        });
        trips.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DashBoardFragment fragment = new DashBoardFragment();
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.container, fragment);
                transaction.addToBackStack("back");
                transaction.commit();
            }
        });
        quotaion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                QuotationFragment fragment = new QuotationFragment();
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.container, fragment);
                transaction.addToBackStack("back");
                transaction.commit();
            }
        });
        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NotificationFragment fragment = new NotificationFragment();
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.container, fragment);
                transaction.addToBackStack("back");
                transaction.commit();
            }
        });
        chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), ChatActivity.class);
                startActivity(i);
            }
        });


        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    public void getTrips(final DatabaseHandler db) {
        String URL = Constants.getTripUrl + getDetails("currentEmail", db);

        GetApi updateProfileApi = new GetApi(
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        getValue(jsonObject.toString(), db);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                if (checkInternetConnection(getActivity())) {

                    new LovelyInfoDialog(getActivity())
                            .setTopColorRes(R.color.colorAccent)
                            .setIcon(R.drawable.ic_tab_infor)

                            //This will add Don't show again checkbox to the dialog. You can pass any ID as argument
                            .setTitleGravity(Gravity.CENTER)
                            .setNotShowAgainOptionChecked(false)
                            .setTitle("Slow Internet!")

                            .setMessage("Slow Internet! Could not fetch latest information\n\nThe information shown above is from offline")
                            .show();

                    getValue(getDetails("completeDetails", db), db);
                } else {

                    new LovelyInfoDialog(getActivity())
                            .setTopColorRes(R.color.colorAccent)
                            .setIcon(R.drawable.ic_tab_infor)
                            .setTitleGravity(Gravity.CENTER)
                            //This will add Don't show again checkbox to the dialog. You can pass any ID as argument

                            .setNotShowAgainOptionChecked(false)
                            .setTitle("No Internet!")

                            .setMessage("No Internet! Could not fetch latest information\n\nThe information shown above is from offline")
                            .show();
                    getValue(getDetails("completeDetails", db), db);
                }

            }
        }, URL);

        App.getVolleyQueue().add(updateProfileApi);

    }

    private void storetoFirebase(final String displayname, final String email) {

       /* mFirebaseDatabaseReference.child("users").setValue(Name);

        startActivity(new Intent(LoginActivity.this, MainActivity.class));
        finish();
*/

        /*final ProgressDialog pd = new ProgressDialog(LoginActivity.this);
        pd.setMessage("Loading...");
        pd.show();*/


        String url = "https://itwtravelgo-3bb51.firebaseio.com/users.json";
        DatabaseHandler db = new DatabaseHandler(getActivity());

        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Firebase reference = new Firebase("https://itwtravelgo-3bb51.firebaseio.com/users");
                final DatabaseHandler db = new DatabaseHandler(getActivity());
                if (s.equals("null")) {
                    reference.child(displayname).child("name").setValue(replaceDisplayName(displayname));
                    reference.child(displayname).child("email").setValue(email);
                    reference.child(displayname).child("count").setValue(0);
                    reference.child(displayname).child("fcmid").setValue(CommonMethodsFragments.getDetails("refreshedToken", db));
                    reference.child(displayname).child("admincount").setValue(0);
                    reference.child(displayname).child("lastmsgtime").setValue(Calendar.getInstance().getTime().getTime());

                    CommonMethodsFragments.addDetails("firebaseDisplayname", replaceDisplayName(displayname), db);
                    CommonMethodsFragments.addDetails("firebaseEmail", email, db);

                   /* Toast.makeText(getActivity(), reference.child("count").getKey() + "", Toast.LENGTH_LONG).show();
                    Toast.makeText(getActivity(), "registration successful", Toast.LENGTH_LONG).show();*/
                } else {
                    try {
                        JSONObject obj = new JSONObject(s);

                        if (!obj.has(email)) {

                            reference.child(displayname).child("name").setValue(replaceDisplayName(displayname));
                            reference.child(displayname).child("email").setValue(email);
                            reference.child(displayname).child("count").setValue(0);
                            reference.child(displayname).child("fcmid").setValue(CommonMethodsFragments.getDetails("refreshedToken", db));
                            reference.child(displayname).child("admincount").setValue(0);
                            reference.child(displayname).child("lastmsgtime").setValue(Calendar.getInstance().getTime().getTime());

                            CommonMethodsFragments.addDetails("firebaseDisplayname", replaceDisplayName(displayname), db);
                            CommonMethodsFragments.addDetails("firebaseEmail", email, db);


                            PrefUtils.setDefaults("email", email, getActivity());
                            PrefUtils.setDefaults("displayname", displayname, getActivity());
                            // Toast.makeText(getBaseContext(),reference.child("count").getKey()+"",Toast.LENGTH_LONG).show();

                            reference.addListenerForSingleValueEvent(new ValueEventListener() {

                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    for (DataSnapshot snap : dataSnapshot.getChildren()) {

                                        if (snap.child("email").getValue().equals(CommonMethodsFragments.getDetails("email", db))) {
                                            // Toast.makeText(getActivity(), snap.child("count").getValue() + "", Toast.LENGTH_LONG).show();
                                            Log.e(snap.getKey(), snap.getChildrenCount() + "");
                                        }
                                    }
                                }

                                @Override
                                public void onCancelled(FirebaseError firebaseError) {

                                }


                            });

                            //  Toast.makeText(getActivity(), "registration successful2", Toast.LENGTH_LONG).show();
                        } else {
                            reference.addListenerForSingleValueEvent(new ValueEventListener() {

                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    for (DataSnapshot snap : dataSnapshot.getChildren()) {
                                        adminChatCount=snap.child("admincount").getValue().toString();
                                        }
                                }

                                @Override
                                public void onCancelled(FirebaseError firebaseError) {

                                }


                            });

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }

            private String replaceDisplayName(String displayname) {
                return displayname.replace(".", "").replace("#", "").replace("$", "").replace("[", "").replace("]", "");
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                System.out.println("" + volleyError);
            }
        });

        RequestQueue rQueue = Volley.newRequestQueue(getActivity());
        rQueue.add(request);
    }


    public void saveFcmId(JSONObject json) {
        String URL = Constants.saveFcmId;

        PostApi api = new PostApi(Request.Method.POST, URL, json, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {

                try {
                    int code = jsonObject.getInt("code");
                    if (code == 200) {
                        //  Toast.makeText(getActivity(), "fcm saved", Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {


            }
        });

        App.getVolleyQueue().add(api);
    }

    public void getValue(String json, DatabaseHandler db) {
        try {
            JSONObject jsonObject = new JSONObject(json);
            JSONObject dat = jsonObject.getJSONObject("data");
            JSONObject users = dat.getJSONObject("users");
            JSONArray data = users.getJSONArray("tripInfos");
            addDetails("tripBasedUserInfo", data.toString(), db);
            JSONArray trips = users.getJSONArray("trips");
            addDetails("userBasedTrip", trips.toString(), db);
            addDetails("completeDetails", json.toString(), db);

            String userName = users.getString("firstName") + " " + users.getString("lastName");
            String userEmail = users.getString("email");

            Dashboard.setUserDetails(userName, userEmail);
            int confirmedCount = 0;
            int quotationsCount = 0;

            for (int i = 0; i < trips.length(); i++) {
                JSONObject tripOject = trips.getJSONObject(i);

                if (tripOject.getString("type").equalsIgnoreCase("confirmed")) {

                    confirmedCount = confirmedCount+1;

                }else if(tripOject.getString("type").equalsIgnoreCase("quotation"))
                {
                    quotationsCount=quotationsCount+1;
                }

            }

            if(confirmedCount>0)
            {
                tripCount.setVisibility(View.VISIBLE);
                tripCount.setText(confirmedCount+"");
                tripCount.setStrokeWidth(1);
                tripCount.setSolidColor("#5c8991");
                tripCount.setStrokeColor("#1e474d");
            }else
            {
                tripCount.setVisibility(View.GONE);
            }

            if(quotationsCount>0)
            {
                quotationCountNew.setVisibility(View.VISIBLE);
                quotationCountNew.setText(quotationsCount+"");
                quotationCountNew.setStrokeWidth(1);
                quotationCountNew.setSolidColor("#5c8991");
                quotationCountNew.setStrokeColor("#1e474d");
            }else
            {
                quotationCountNew.setVisibility(View.GONE);
            }

            if(CommonMethodsFragments.getDetails("messageCount",db)!=null) {
                adminChatCount = CommonMethodsFragments.getDetails("messageCount", db);
                if (!adminChatCount.equalsIgnoreCase("0")&&!adminChatCount.equalsIgnoreCase("")) {
                    chatCount.setVisibility(View.VISIBLE);
                    chatCount.setText(adminChatCount);
                    chatCount.setStrokeWidth(1);
                    chatCount.setSolidColor("#5c8991");
                    chatCount.setStrokeColor("#1e474d");
                }else {
                    chatCount.setVisibility(View.GONE);
                }
            }

            if(CommonMethodsFragments.getDetails("notificationCount",db)!=null) {
                notificationMessageCount = CommonMethodsFragments.getDetails("notificationCount", db);
                if (!notificationMessageCount.equalsIgnoreCase("0")&&!notificationMessageCount.equalsIgnoreCase("")) {
                    notificationCount.setVisibility(View.VISIBLE);
                    notificationCount.setText(notificationMessageCount);
                    notificationCount.setStrokeWidth(1);
                    notificationCount.setSolidColor("#5c8991");
                    notificationCount.setStrokeColor("#1e474d");
                }else
                {
                    notificationCount.setVisibility(View.GONE);
                }
            }

            JSONObject json2 = new JSONObject();

            try {
                json2.put("email", CommonMethodsFragments.getDetails("currentEmail", db));
                json2.put("fcmId", CommonMethodsFragments.getDetails("refreshedToken", db));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            saveFcmId(json2);
            storetoFirebase(users.getString("firstName") + " " + users.getString("lastName"), getDetails("currentEmail", db));


        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public void onEvent(EventBusModel json) {

        DatabaseHandler db=new DatabaseHandler(getActivity());
        getValue(json.getJson(),db);
    }

    @Override
    public void onDestroy() {

        EventBus.getDefault().unregister(this);
        getActivity().stopService(new Intent(getActivity(), MyService.class));
        super.onDestroy();
    }

    @Override
    public void onPause() {

        EventBus.getDefault().unregister(this);
        getActivity().stopService(new Intent(getActivity(), MyService.class));
        super.onPause();
    }

    @Override
    public void onResume() {


        getActivity().startService(new Intent(getActivity(), MyService.class));
        EventBus.getDefault().register(this);
        super.onResume();
    }
}
