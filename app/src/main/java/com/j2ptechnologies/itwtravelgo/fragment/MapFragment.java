package com.j2ptechnologies.itwtravelgo.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.j2ptechnologies.itwtravelgo.R;
import com.j2ptechnologies.itwtravelgo.Utils.CustomItemClickListener;
import com.j2ptechnologies.itwtravelgo.Utils.GPSTracker;
import com.j2ptechnologies.itwtravelgo.adapter.MapSearchAdapter;
import com.j2ptechnologies.itwtravelgo.api.GetApi;
import com.j2ptechnologies.itwtravelgo.app.App;
import com.j2ptechnologies.itwtravelgo.model.MapSearchModel;
import com.j2ptechnologies.itwtravelgo.util.CustomDialog;
import com.j2ptechnologies.itwtravelgo.util.MyMapView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class MapFragment extends Fragment {

    MyMapView mMapView;
    private GoogleMap googleMap;
    Button button;
    FloatingActionButton search;
    ArrayList<MapSearchModel> mapSearch;
    double lat, lng;
    GPSTracker gpsTracker;
    CustomDialog custmdlg;

    public MapFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_map_search, container, false);
        mapSearch = new ArrayList<>();
        gpsTracker = new GPSTracker(getActivity());
        lat = gpsTracker.getLatitude();
        lng = gpsTracker.getLongitude();

        View bottomSheet = rootView.findViewById(R.id.design_bottom_sheet);
        final BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_DRAGGING:
                        button.setText("SEARCH");
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_DRAGGING");
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        button.setText("SEARCH");
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_SETTLING");
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        button.setText("CLOSE");
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_EXPANDED");
                        break;
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        button.setText("SEARCH");
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_COLLAPSED");
                        break;
                    case BottomSheetBehavior.STATE_HIDDEN:
                        button.setText("SEARCH");
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_HIDDEN");
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                Log.i("BottomSheetCallback", "slideOffset: " + slideOffset);
            }
        });

        search = (FloatingActionButton) rootView.findViewById(R.id.search);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (behavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                    button.setText("CLOSE");
                } else {
                    behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    button.setText("SEARCH");
                }
            }
        });
        button = (Button) rootView.findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (behavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                    button.setText("CLOSE");
                } else {
                    behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    button.setText("SEARCH");
                }
            }
        });

        mMapView = (MyMapView) rootView.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }


        mMapView.getMapAsync(new OnMapReadyCallback() {
            @SuppressLint("MissingPermission")
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;

                // For showing a move to my location button
                googleMap.setMyLocationEnabled(true);
               /* googleMap.getUiSettings().setZoomControlsEnabled(false);
                googleMap.getUiSettings().setAllGesturesEnabled(false);*/

                googleMap.getUiSettings().setZoomControlsEnabled(true);
                googleMap.getUiSettings().setRotateGesturesEnabled(true);
                googleMap.getUiSettings().setScrollGesturesEnabled(true);
                googleMap.getUiSettings().setTiltGesturesEnabled(true);

                // For dropping a marker at a point on the Map
                LatLng sydney = new LatLng(lat, lng);
                googleMap.addMarker(new MarkerOptions().position(sydney).title("Marker Title").snippet("Marker Description"));

                // For zooming automatically to the location of the marker
                CameraPosition cameraPosition = new CameraPosition.Builder().target(sydney).zoom(15).build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {
                        showpopup(lat, lng, marker.getPosition().latitude, marker.getPosition().longitude);
                        /*Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                                Uri.parse("http://maps.google.com/maps?saddr="+lat+","+lng+"&daddr="+marker.getPosition().latitude+","+marker.getPosition().longitude));
                        startActivity(intent);*/
                        return false;
                    }
                });
            }
        });

        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.card_recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(), 6);
        recyclerView.setLayoutManager(layoutManager);

        final ArrayList<MapSearchModel> androidVersions = prepareMapSearch();
        MapSearchAdapter adapter = new MapSearchAdapter(getActivity(), androidVersions, new CustomItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                lat = gpsTracker.getLatitude();
                lng = gpsTracker.getLongitude();
                googleMap.clear();
                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                button.setText("SEARCH");
                getPlaces(androidVersions.get(position).getSearchName(), lat, lng);


            }
        });
        recyclerView.setAdapter(adapter);

        return rootView;
    }


    public ArrayList<MapSearchModel> prepareMapSearch() {
        mapSearch.clear();
        String title[] = {
                "Airport",
                "ATM",
                "Bus Station",
                "Cafe",
                "Casino",
                "Church",
                "Doctor",
                "Hindu Temple",
                "Hospital",
                "Mosque",
                "Movie Theatre",
                "Parking",
                "Police",
                "Stadium",
                "Train Station",
                "Taxi Stand",
                "Night Club",
                "Travel Agency"
        };

        String searchName[] = {
                "airport",
                "atm",
                "bus_station",
                "cafe",
                "casino",
                "church",
                "doctor",
                "hindu_temple",
                "hospital",
                "mosque",
                "movie_theater",
                "parking",
                "police",
                "stadium",
                "train_station",
                "taxi_stand",
                "night_club",
                "travel_agency"
        };

        int drawable[] = {
                R.drawable.airplane, R.drawable.atm, R.drawable.school_bus, R.drawable.cafe, R.drawable.croupier, R.drawable.church, R.drawable.doctor,
                R.drawable.temple, R.drawable.hospital, R.drawable.mosque, R.drawable.film_reel, R.drawable.car, R.drawable.police, R.drawable.soccer_field,
                R.drawable.train_new, R.drawable.taxi, R.drawable.night_club, R.drawable.travel_new

        };


        for (int i = 0; i < title.length; i++) {
            MapSearchModel mpS = new MapSearchModel(drawable[i], title[i], searchName[i]);
            mapSearch.add(mpS);
        }
        return mapSearch;
    }


    public void getPlaces(String placeName, double lat, final double lng) {
        String URL = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + lat + "," + lng + "&rankby=distance&type=" + placeName + "&key=AIzaSyDMdWxl64uv2YZi0wwWO3pqO_Kcrehi7Fg";

        GetApi updateProfileApi = new GetApi(
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {

                        try {
                            JSONArray array = jsonObject.getJSONArray("results");
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject jo = array.getJSONObject(i);
                                JSONObject geo = jo.getJSONObject("geometry");
                                JSONObject location = geo.getJSONObject("location");

                                double lt = location.getDouble("lat");
                                double lg = location.getDouble("lng");


                                String iconUrl = jo.getString("icon");
                                String name = jo.getString("name");

                                String title = "";

                                boolean open_now = false;
                                try {
                                    JSONObject opening_hours = jo.getJSONObject("opening_hours");
                                    open_now = opening_hours.getBoolean("open_now");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                if (open_now) {
                                    title = name + "- OPEN";

                                } else {
                                    title = name + "- CLOSED";
                                }

                                getImage(iconUrl, title, lt, lg);

                              /*  googleMap.setMyLocationEnabled(true);

                                // For dropping a marker at a point on the Map
                                LatLng sydney = new LatLng(lt, lg);
                                googleMap.addMarker(new MarkerOptions().position(sydney).title(title));
                                googleMap.getUiSettings().setZoomControlsEnabled(true);
                                googleMap.getUiSettings().setAllGesturesEnabled(true);
                                // For zooming automatically to the location of the marker
                                CameraPosition cameraPosition = new CameraPosition.Builder().target(sydney).zoom(12).build();
                                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));*/

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {


            }
        }, URL);

        App.getVolleyQueue().add(updateProfileApi);

    }


    public void getImage(String url, final String title, final double lat, final double lng) {

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());

        // Initialize a new ImageRequest
        ImageRequest imageRequest = new ImageRequest(url, new Response.Listener<Bitmap>() {
            @Override
            public void onResponse(Bitmap response) {
                googleMap.setMyLocationEnabled(true);

                // For dropping a marker at a point on the Map
                LatLng sydney = new LatLng(lat, lng);
                googleMap.addMarker(new MarkerOptions().position(sydney).title(title).icon(BitmapDescriptorFactory.fromBitmap(response)));
                googleMap.getUiSettings().setZoomControlsEnabled(true);
                googleMap.getUiSettings().setAllGesturesEnabled(true);
                // For zooming automatically to the location of the marker
                CameraPosition cameraPosition = new CameraPosition.Builder().target(sydney).zoom(15).build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            }
        },
                0, // Image width
                0, // Image height
                // Image scale type
                Bitmap.Config.RGB_565, //Image decode configuration
                new Response.ErrorListener() { // Error listener
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Do something with error response
                        error.printStackTrace();
                    }
                }
        );

        // Add ImageRequest to the RequestQueue
        requestQueue.add(imageRequest);


     /*
        ImageRequest request = new ImageRequest(url,
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap bitmap) {
                        googleMap.setMyLocationEnabled(true);

                        // For dropping a marker at a point on the Map
                        LatLng sydney = new LatLng(lat, lng);
                        googleMap.addMarker(new MarkerOptions().position(sydney).title(title));

                        // For zooming automatically to the location of the marker
                        CameraPosition cameraPosition = new CameraPosition.Builder().target(sydney).zoom(12).build();
                        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                    }
                }, 0, 0, null,
                new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {

                    }
                });*/
    }

    public void showpopup(final double lat, final double longi, final double marklat, final double marklongitude) {
        custmdlg = new CustomDialog(getActivity());
        custmdlg.requestWindowFeature(Window.FEATURE_NO_TITLE);
        custmdlg.setContentView(R.layout.openmaplayout);
        WindowManager.LayoutParams a = custmdlg.getWindow().getAttributes();
        a.gravity = Gravity.CENTER;
        custmdlg.getWindow().setAttributes(a);

        custmdlg.setCancelable(false);
        custmdlg.getWindow().setLayout(ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.WRAP_CONTENT);

        custmdlg.show();

        Button btncancel = custmdlg.findViewById(R.id.btncancel);
        Button btnsubmit = custmdlg.findViewById(R.id.btnsubmit);

        btncancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                custmdlg.dismiss();
            }
        });
        btnsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?saddr=" + lat + "," + longi + "&daddr=" + marklat + "," + marklongitude));
                startActivity(intent);
                custmdlg.dismiss();
            }
        });
    }
}
