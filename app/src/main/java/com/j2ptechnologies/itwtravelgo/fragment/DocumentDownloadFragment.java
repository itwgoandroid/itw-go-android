package com.j2ptechnologies.itwtravelgo.fragment;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.j2ptechnologies.itwtravelgo.R;
import com.j2ptechnologies.itwtravelgo.Utils.CommonMethodsFragments;
import com.j2ptechnologies.itwtravelgo.Utils.FileDownloader;
import com.j2ptechnologies.itwtravelgo.db.DatabaseHandler;

import java.io.File;
import java.io.IOException;


public class DocumentDownloadFragment extends CommonMethodsFragments {

    String fileNames;
    CardView card_view;
    TextView validTill;

    public DocumentDownloadFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_four, container, false);

        final DatabaseHandler db=new DatabaseHandler(getActivity());

        card_view=(CardView)view.findViewById(R.id.card_view);
        validTill=(TextView) view.findViewById(R.id.validTill);
        validTill.setText(getDetails("currentTripDate",db));

        card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                download(getDetails("currentDocumentPath",db),"TripSummary.pdf");
            }
        });

        return view;
    }


    public void download(String url,String fileName)
    {
        fileNames=fileName;
        new DownloadFile().execute(url, fileName);
    }

    public void view()
    {
        File pdfFile = new File(Environment.getExternalStorageDirectory() + "/KIC_Downloads/" + fileNames);  // -> filename = maven.pdf
        Uri path = Uri.fromFile(pdfFile);
        Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
        pdfIntent.setDataAndType(path, "application/pdf");
        pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        try{
            startActivity(pdfIntent);
        }catch(ActivityNotFoundException e){
            Toast.makeText(getActivity(), "No Application available to view PDF", Toast.LENGTH_SHORT).show();
        }
    }

    private class DownloadFile extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... strings) {
            String fileUrl = strings[0];
            String fileName = strings[1];
            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
            File folder = new File(extStorageDirectory, "KIC_Downloads");
            folder.mkdir();

            File pdfFile = new File(folder, fileName);

            try{
                pdfFile.createNewFile();
            }catch (IOException e){
                e.printStackTrace();
            }
            FileDownloader.downloadFile(fileUrl, pdfFile);
            return null;
        }

        @Override
        protected void onPreExecute() {
            Toast.makeText(getActivity(),"Downloading... Please wait",Toast.LENGTH_LONG).show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            view();
            super.onPostExecute(aVoid);
        }
    }

}
