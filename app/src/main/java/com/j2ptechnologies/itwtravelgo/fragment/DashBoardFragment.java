package com.j2ptechnologies.itwtravelgo.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.j2ptechnologies.itwtravelgo.Activity.Dashboard;
import com.j2ptechnologies.itwtravelgo.R;
import com.j2ptechnologies.itwtravelgo.Utils.CatLoadingView;
import com.j2ptechnologies.itwtravelgo.Utils.CommonMethodsFragments;
import com.j2ptechnologies.itwtravelgo.Utils.Constants;
import com.j2ptechnologies.itwtravelgo.Utils.LovelyInfoDialog;
import com.j2ptechnologies.itwtravelgo.adapter.DashboardTripListAdapter;
import com.j2ptechnologies.itwtravelgo.api.GetApi;
import com.j2ptechnologies.itwtravelgo.api.PostApi;
import com.j2ptechnologies.itwtravelgo.app.App;
import com.j2ptechnologies.itwtravelgo.db.DatabaseHandler;
import com.j2ptechnologies.itwtravelgo.model.DashboardTripsListModel;
import com.j2ptechnologies.itwtravelgo.util.PrefUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DashBoardFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DashBoardFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DashBoardFragment extends CommonMethodsFragments {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    RecyclerView recyclerView;
    DatabaseHandler db;
    public List<DashboardTripsListModel> rowItems;
    DashboardTripListAdapter adapter;
    private OnFragmentInteractionListener mListener;
    CatLoadingView mView;

    public DashBoardFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DashBoardFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DashBoardFragment newInstance(String param1, String param2) {
        DashBoardFragment fragment = new DashBoardFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        db = new DatabaseHandler(getActivity());
        rowItems = new ArrayList<DashboardTripsListModel>();
        mView = new CatLoadingView();
        recyclerView = (RecyclerView) view.findViewById(R.id.card_recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        adapter = new DashboardTripListAdapter(getActivity(), (ArrayList<DashboardTripsListModel>) rowItems, db);
        getTrips(db);
        recyclerView.setAdapter(adapter);

        /*ImageView one=(ImageView)view.findViewById(R.id.image);
        one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(getActivity(),TripDetailsActivity.class);
                startActivity(i);
            }
        });*/
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

   /* public void getJson(DatabaseHandler db)
    {
        try {
            JSONObject json=new JSONObject(loadJSONFromAsset());
            getValue(json.toString(),db);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getActivity().getAssets().open("json.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }*/

    public void getValue(String json, DatabaseHandler db) {
        try {
            JSONObject jsonObject = new JSONObject(json);
            JSONObject dat = jsonObject.getJSONObject("data");
            JSONObject users = dat.getJSONObject("users");
            JSONArray data = users.getJSONArray("tripInfos");
            addDetails("tripBasedUserInfo", data.toString(), db);
            JSONArray trips = users.getJSONArray("trips");
            addDetails("userBasedTrip", trips.toString(), db);
            addDetails("completeDetails", json.toString(), db);

            String userName = users.getString("firstName") + " " + users.getString("lastName");
            String userEmail = users.getString("email");

            Dashboard.setUserDetails(userName, userEmail);

            for (int i = 0; i < trips.length(); i++) {
                JSONObject tripOject = trips.getJSONObject(i);
                String id = tripOject.getString("id");
                String name = tripOject.getString("name");
                String startDate = tripOject.getString("startDate");
                String endDate = tripOject.getString("endDate");
                String imageUrl = Constants.baseUrl + tripOject.getString("imagePath");
                String documentUrl = Constants.baseUrl + tripOject.getString("documentPath");
                if (tripOject.getString("type").equalsIgnoreCase("confirmed")) {
                    JSONArray tripInfos = users.getJSONArray("tripInfos");
                    for (int k = 0; k < tripInfos.length(); k++) {
                        JSONObject tripInfo = tripInfos.getJSONObject(k);
                        if (tripInfo.getString("tripId").equals(id)) {

                            DashboardTripsListModel dashboardTripsListModel = new DashboardTripsListModel(id, name, changeDateNew(startDate), changeDateNew(endDate), imageUrl, tripInfo, documentUrl);
                            rowItems.add(dashboardTripsListModel);

                        }
                    }

                }


            }


            adapter.notifyDataSetChanged();

            JSONObject json2 = new JSONObject();

            try {
                json2.put("email", CommonMethodsFragments.getDetails("currentEmail", db));
                json2.put("fcmId", CommonMethodsFragments.getDetails("refreshedToken", db));
            } catch (JSONException e) {
                e.printStackTrace();
            }

           // saveFcmId(json2);
            //storetoFirebase(users.getString("firstName") + " " + users.getString("lastName"), getDetails("currentEmail", db));


        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public void showDialog() {
        mView.setClickCancelAble(false);
        mView.show(getFragmentManager(), "");
    }

    public void dismissDialog() {
        mView.dismiss();
    }

    public void getTrips(final DatabaseHandler db) {
        showDialog();
        String URL = Constants.getTripUrl + getDetails("currentEmail", db);

        GetApi updateProfileApi = new GetApi(
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        dismissDialog();
                        getValue(jsonObject.toString(), db);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                dismissDialog();

                if (checkInternetConnection(getActivity())) {

                    new LovelyInfoDialog(getActivity())
                            .setTopColorRes(R.color.colorAccent)
                            .setIcon(R.drawable.ic_tab_infor)

                            //This will add Don't show again checkbox to the dialog. You can pass any ID as argument
                            .setTitleGravity(Gravity.CENTER)
                            .setNotShowAgainOptionChecked(false)
                            .setTitle("Slow Internet!")

                            .setMessage("Slow Internet! Could not fetch latest information\n\nThe information shown above is from offline")
                            .show();

                    getValue(getDetails("completeDetails", db), db);
                } else {

                    new LovelyInfoDialog(getActivity())
                            .setTopColorRes(R.color.colorAccent)
                            .setIcon(R.drawable.ic_tab_infor)
                            .setTitleGravity(Gravity.CENTER)
                            //This will add Don't show again checkbox to the dialog. You can pass any ID as argument

                            .setNotShowAgainOptionChecked(false)
                            .setTitle("No Internet!")

                            .setMessage("No Internet! Could not fetch latest information\n\nThe information shown above is from offline")
                            .show();
                    getValue(getDetails("completeDetails", db), db);
                }

            }
        }, URL);

        App.getVolleyQueue().add(updateProfileApi);

    }

    /*private void storetoFirebase(final String displayname, final String email) {

       *//* mFirebaseDatabaseReference.child("users").setValue(Name);

        startActivity(new Intent(LoginActivity.this, MainActivity.class));
        finish();
*//*

        *//*final ProgressDialog pd = new ProgressDialog(LoginActivity.this);
        pd.setMessage("Loading...");
        pd.show();*//*

        showDialog();

        String url = "https://itwtravelgo-3bb51.firebaseio.com/users.json";
        DatabaseHandler db = new DatabaseHandler(getActivity());

        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Firebase reference = new Firebase("https://itwtravelgo-3bb51.firebaseio.com/users");
                final DatabaseHandler db = new DatabaseHandler(getActivity());
                if (s.equals("null")) {
                    reference.child(displayname).child("name").setValue(replaceDisplayName(displayname));
                    reference.child(displayname).child("email").setValue(email);
                    reference.child(displayname).child("count").setValue(0);
                    reference.child(displayname).child("fcmid").setValue(CommonMethodsFragments.getDetails("refreshedToken", db));
                    reference.child(displayname).child("admincount").setValue(0);
                    reference.child(displayname).child("lastmsgtime").setValue(Calendar.getInstance().getTime().getTime());

                    CommonMethodsFragments.addDetails("firebaseDisplayname", replaceDisplayName(displayname), db);
                    CommonMethodsFragments.addDetails("firebaseEmail", email, db);

                   *//* Toast.makeText(getActivity(), reference.child("count").getKey() + "", Toast.LENGTH_LONG).show();
                    Toast.makeText(getActivity(), "registration successful", Toast.LENGTH_LONG).show();*//*
                } else {
                    try {
                        JSONObject obj = new JSONObject(s);

                        if (!obj.has(email)) {

                            reference.child(displayname).child("name").setValue(replaceDisplayName(displayname));
                            reference.child(displayname).child("email").setValue(email);
                            reference.child(displayname).child("count").setValue(0);
                            reference.child(displayname).child("fcmid").setValue(CommonMethodsFragments.getDetails("refreshedToken", db));
                            reference.child(displayname).child("admincount").setValue(0);
                            reference.child(displayname).child("lastmsgtime").setValue(Calendar.getInstance().getTime().getTime());

                            CommonMethodsFragments.addDetails("firebaseDisplayname", replaceDisplayName(displayname), db);
                            CommonMethodsFragments.addDetails("firebaseEmail", email, db);


                            PrefUtils.setDefaults("email", email, getActivity());
                            PrefUtils.setDefaults("displayname", displayname, getActivity());
                            // Toast.makeText(getBaseContext(),reference.child("count").getKey()+"",Toast.LENGTH_LONG).show();

                            reference.addListenerForSingleValueEvent(new ValueEventListener() {

                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    for (DataSnapshot snap : dataSnapshot.getChildren()) {
                                        if (snap.child("email").getValue().equals(CommonMethodsFragments.getDetails("email", db))) {
                                            // Toast.makeText(getActivity(), snap.child("count").getValue() + "", Toast.LENGTH_LONG).show();
                                            Log.e(snap.getKey(), snap.getChildrenCount() + "");
                                        }
                                    }
                                }

                                @Override
                                public void onCancelled(FirebaseError firebaseError) {

                                }


                            });

                            //  Toast.makeText(getActivity(), "registration successful2", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getActivity(), "username already exists", Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                dismissDialog();
            }

            private String replaceDisplayName(String displayname) {
                return displayname.replace(".", "").replace("#", "").replace("$", "").replace("[", "").replace("]", "");
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                System.out.println("" + volleyError);
                dismissDialog();
            }
        });

        RequestQueue rQueue = Volley.newRequestQueue(getActivity());
        rQueue.add(request);
    }*/


   /* public void saveFcmId(JSONObject json) {
        String URL = Constants.saveFcmId;

        PostApi api = new PostApi(Request.Method.POST, URL, json, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {

                try {
                    int code = jsonObject.getInt("code");
                    if (code == 200) {
                        //  Toast.makeText(getActivity(), "fcm saved", Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {


            }
        });

        App.getVolleyQueue().add(api);
    }*/

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    MainScreenFragment fragment = new MainScreenFragment();
                    FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.container, fragment);
                    transaction.addToBackStack("back");
                    transaction.commit();
                    return true;
                }
                return false;
            }
        });
    }
}
