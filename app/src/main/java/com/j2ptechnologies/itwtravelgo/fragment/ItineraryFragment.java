package com.j2ptechnologies.itwtravelgo.fragment;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.j2ptechnologies.itwtravelgo.R;
import com.j2ptechnologies.itwtravelgo.Utils.CatLoadingView;
import com.j2ptechnologies.itwtravelgo.Utils.CommonMethodsFragments;
import com.j2ptechnologies.itwtravelgo.adapter.ItineraryAdapter;
import com.j2ptechnologies.itwtravelgo.db.DatabaseHandler;
import com.j2ptechnologies.itwtravelgo.model.ItineraryModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class ItineraryFragment extends CommonMethodsFragments {

    List<ItineraryModel> rowItems;
    List<JSONObject> obj = new ArrayList<>();
    RecyclerView list;
    ItineraryAdapter mAdapter;
    DatabaseHandler db;
    CatLoadingView mView;
    public ItineraryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_itinerary, container, false);
        db = new DatabaseHandler(getActivity());
        rowItems = new ArrayList<ItineraryModel>();
        mView = new CatLoadingView();
        list = (RecyclerView) view.findViewById(R.id.list);
        mAdapter = new ItineraryAdapter(rowItems, getActivity());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        list.setLayoutManager(mLayoutManager);
        list.setItemAnimator(new DefaultItemAnimator());
        list.setAdapter(mAdapter);
showDialog();
        loadItinerary(getDetails("completeDetails", db), db);



        return view;
    }


    public void loadItinerary(String jsn, DatabaseHandler db) {
        JSONObject jsonNew = null;
        try {
            jsonNew = new JSONObject(jsn);
            JSONObject dat = jsonNew.getJSONObject("data");
            JSONObject users = dat.getJSONObject("users");
            rowItems.clear();
            JSONArray trips = users.getJSONArray("trips");
            for (int m = 0; m < trips.length(); m++) {
                JSONObject json = trips.getJSONObject(m);
                if (json.getString("id").equals(getDetails("currentTripId", db))) {

                    try {
                        JSONArray flightDetails = json.getJSONArray("flights");
                        for (int i = 0; i < flightDetails.length(); i++) {
                            JSONObject flightObject = flightDetails.getJSONObject(i);
                            ItineraryModel itModel = new ItineraryModel(1, flightObject, new JSONObject(getDetails("currentTripUserObject", db)));
                            rowItems.add(itModel);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    try {
                        JSONArray accomodationDetails = json.getJSONArray("accomodations");
                        for (int i = 0; i < accomodationDetails.length(); i++) {
                            JSONObject accomodationObject = accomodationDetails.getJSONObject(i);
                            obj.add(accomodationObject);
                            ItineraryModel itModel = new ItineraryModel(2, accomodationObject, new JSONObject(getDetails("currentTripUserObject", db)));
                            rowItems.add(itModel);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    try {
                        JSONArray tripsDetails = json.getJSONArray("transports");
                        for (int i = 0; i < tripsDetails.length(); i++) {
                            JSONObject transportationObject = tripsDetails.getJSONObject(i);
                            obj.add(transportationObject);
                            ItineraryModel itModel = new ItineraryModel(3, transportationObject, new JSONObject(getDetails("currentTripUserObject", db)));
                            rowItems.add(itModel);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    try {
                        JSONArray activityDetails = json.getJSONArray("activities");
                        for (int i = 0; i < activityDetails.length(); i++) {
                            JSONObject activityObject = activityDetails.getJSONObject(i);
                            obj.add(activityObject);
                            ItineraryModel itModel = new ItineraryModel(4, activityObject, new JSONObject(getDetails("currentTripUserObject", db)));
                            rowItems.add(itModel);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    try {
                        JSONArray insuranceDetails = json.getJSONArray("insurances");
                        for (int i = 0; i < insuranceDetails.length(); i++) {
                            JSONObject insuranceObject = insuranceDetails.getJSONObject(i);
                            obj.add(insuranceObject);
                            ItineraryModel itModel = new ItineraryModel(5, insuranceObject, new JSONObject(getDetails("currentTripUserObject", db)));
                            rowItems.add(itModel);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    dismissDialog();
                    mAdapter.notifyDataSetChanged();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void showDialog() {
        mView.setClickCancelAble(false);
        FragmentManager manager = getActivity().getSupportFragmentManager();
        mView.show(manager, "");
    }
    public void dismissDialog() {
        mView.dismiss();
    }

}
