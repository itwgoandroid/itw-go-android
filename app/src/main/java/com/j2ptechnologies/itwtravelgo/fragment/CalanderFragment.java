package com.j2ptechnologies.itwtravelgo.fragment;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.MapsInitializer;
import com.j2ptechnologies.itwtravelgo.R;
import com.j2ptechnologies.itwtravelgo.Utils.CommonMethodsFragments;
import com.j2ptechnologies.itwtravelgo.Utils.FileDownloader;
import com.j2ptechnologies.itwtravelgo.calendar.CustomCalendar;
import com.j2ptechnologies.itwtravelgo.calendar.dao.EventData;
import com.j2ptechnologies.itwtravelgo.calendar.dao.dataAboutDate;
import com.j2ptechnologies.itwtravelgo.db.DatabaseHandler;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;


public class CalanderFragment extends CommonMethodsFragments {
    private CustomCalendar customCalendars;
    ArrayList<String> arr = new ArrayList<String>();



    public CalanderFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_calander, container, false);
        customCalendars = (CustomCalendar) view.findViewById(R.id.customCalendar);


        for (int i = 0; i < getEventDataList().size(); i++) {
            int eventCount = 3;
            customCalendars.addAnEvent(arr.get(i), eventCount, getEventDataList());
        }
        final DatabaseHandler db=new DatabaseHandler(getActivity());


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public ArrayList<EventData> getEventDataList() {
        ArrayList<EventData> eventDataList = new ArrayList();
        ArrayList<dataAboutDate> dataAboutDates = new ArrayList();
        dataAboutDate dataAboutDate=new dataAboutDate();
        dataAboutDate.setTitle("TestTitle");
        dataAboutDate.setSubject("TestSubject");
        dataAboutDate.setRemarks("TestRemark");
        dataAboutDate.setSubmissionDate("TestSubmissionDate");
        arr.add("2018-04-10");
        dataAboutDates.add(dataAboutDate);
        EventData eventData=new EventData();
        eventData.setSection("TestEvenSection");
        eventData.setData(dataAboutDates);
        eventDataList.add(eventData);
        return eventDataList;
    }
}
