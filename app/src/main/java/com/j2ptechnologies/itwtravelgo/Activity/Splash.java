package com.j2ptechnologies.itwtravelgo.Activity;

import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.j2ptechnologies.itwtravelgo.R;
import com.j2ptechnologies.itwtravelgo.Utils.CommonMethodsFragments;
import com.j2ptechnologies.itwtravelgo.db.DatabaseHandler;
import com.j2ptechnologies.itwtravelgo.util.Util;
import com.j2ptechnologies.itwtravelgo.view.LoginActivity;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;




public class Splash extends AppCompatActivity {
    DatabaseHandler db = new DatabaseHandler(this);

    private static final int    PERMISSIONS_REQUEST = 1234;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        setContentView(R.layout.activity_splash);
        Util.getSoftButtonsBarSizePort(Splash.this);

        try {
            getSupportActionBar().hide();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (Build.VERSION.SDK_INT >= 23) {
            checkPermissions();
        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    transition();
                }
            }, 3000);
        }



    }

    @TargetApi(23)
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST) {
            checkPermissions();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void checkPermissions() {
        String[] ungrantedPermissions = requiredPermissionsStillNeeded();
        if (ungrantedPermissions.length == 0) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    transition();
                }
            }, 3000);
        } else {
            requestPermissions(ungrantedPermissions, PERMISSIONS_REQUEST);
        }
    }

    @TargetApi(23)
    private String[] requiredPermissionsStillNeeded() {

        Set<String> permissions = new HashSet<String>();
        for (String permission : getRequiredPermissions()) {
            permissions.add(permission);
        }
        for (Iterator<String> i = permissions.iterator(); i.hasNext();) {
            String permission = i.next();
            if (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED) {
                Log.d(Splash.class.getSimpleName(),
                        "Permission: " + permission + " already granted.");
                i.remove();
            } else {
                Log.d(Splash.class.getSimpleName(),
                        "Permission: " + permission + " not yet granted.");
            }
        }
        return permissions.toArray(new String[permissions.size()]);
    }

    public String[] getRequiredPermissions() {
        String[] permissions = null;
        try {
            permissions = getPackageManager().getPackageInfo(getPackageName(),
                    PackageManager.GET_PERMISSIONS).requestedPermissions;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        if (permissions == null) {
            return new String[0];
        } else {
            return permissions.clone();
        }
    }


    @Override
    protected void onDestroy() {
        db.close();
        super.onDestroy();
    }




    public void transition(){
        if(isRegistered())
        // if(true)
        {
            Intent i=new Intent(getBaseContext(), Dashboard.class);
            i.putExtra("from","splash");
            finish();
            startActivity(i);

        }else
        {
            Intent i=new Intent(getBaseContext(), LoginActivity.class);
            finish();
            startActivity(i);
        }
    }

    private boolean isRegistered() {
        if(CommonMethodsFragments.getDetails("isRegistered",db)!=null&&CommonMethodsFragments.getDetails("isRegistered",db).equalsIgnoreCase("true"))
        {
            return true;
        }else
        {
            return false;
        }
    }

}