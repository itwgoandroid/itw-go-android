package com.j2ptechnologies.itwtravelgo.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.html.WebColors;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import com.j2ptechnologies.itwtravelgo.R;
import com.j2ptechnologies.itwtravelgo.Utils.CommonMethodsFragments;
import com.j2ptechnologies.itwtravelgo.db.DatabaseHandler;
import com.j2ptechnologies.itwtravelgo.sendmail.GMailSender;
import com.j2ptechnologies.itwtravelgo.util.Util;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class EnquiryActivity extends AppCompatActivity implements View.OnClickListener, DatePickerDialog.OnDateSetListener {

    private EditText edt_destination, edt_departurecity, edt_email, edt_phonenumber;
    private Button btnfixed, btnflexible, btnanytime, btnsubmit;
    private int slctdTime = 0;
    private File filetenant;
    String startTripDate="Start Date Not Selected";
    String endTripDate="End Date Not Selected";
    private DatePickerDialog dpd;
    String datePickerType="from";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_layout);
        Util.getSoftButtonsBarSizePort(EnquiryActivity.this);
        viewInitilaizations();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(Html.fromHtml("<small>Enquire</small>"));
        btnsubmit.setOnClickListener(this);
        btnfixed.setOnClickListener(this);
        btnflexible.setOnClickListener(this);
        btnanytime.setOnClickListener(this);

        DatabaseHandler db = new DatabaseHandler(this);
        edt_email.setText(CommonMethodsFragments.getDetails("currentEmail", db));
    }

    private void viewInitilaizations() {
        edt_destination = findViewById(R.id.edt_destination);
        edt_departurecity = findViewById(R.id.edt_departurecity);
        edt_email = findViewById(R.id.edt_email);
        edt_phonenumber = findViewById(R.id.edt_phonenumber);
        btnsubmit = findViewById(R.id.btnsubmit);

        btnflexible = findViewById(R.id.btnflexible);
        btnfixed = findViewById(R.id.btnfixed);
        btnanytime = findViewById(R.id.btnanytime);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnsubmit:

                sendmail();
                break;
            case R.id.btnfixed:
                fixedselected();
                break;
            case R.id.btnflexible:
                flexibleselected();
                break;
            case R.id.btnanytime:
                anytimeSelected();
                break;
        }
    }

    private void sendmail() {

        String usermail = edt_email.getText().toString().trim();
        String usermobile = edt_phonenumber.getText().toString().trim();
        String strdestination = edt_destination.getText().toString().trim();
        String strdepsrturecity = edt_departurecity.getText().toString().trim();
        String strdate = null;
        if (slctdTime == 0) {
            strdate = "Not Selected";
        } else if (slctdTime == 1) {
            strdate = "Fixed Date ("+startTripDate+" - "+endTripDate+")";
        } else if (slctdTime == 2) {
            strdate = "Flexible date ("+startTripDate+" - "+endTripDate+")";
        } else if (slctdTime == 3) {
            strdate = "Any Time";
        }

        filetenant = new File(Environment.getExternalStorageDirectory()
                .getAbsolutePath() + "/ITW");

        if (!filetenant.exists()) {
            filetenant.mkdir();
        }

        File flepath = new File(filetenant.getAbsolutePath() + "/" + "ITWENQUIRE_01" + ".pdf");
        String filedr = flepath.getAbsolutePath();

        if (usermail.equals("")) {
            Toast.makeText(this, "Please enter correct Email", Toast.LENGTH_SHORT).show();
        } else if (strdestination.equals("")) {
            Toast.makeText(this, "Please enter correct Destination", Toast.LENGTH_SHORT).show();
        } else if (strdepsrturecity.equals("")) {
            Toast.makeText(this, "Please enter correct Departure City", Toast.LENGTH_SHORT).show();
        } else if ((usermobile.length() < 10) || usermobile.equals("")) {
            Toast.makeText(this, "Please enter correct mobile number...", Toast.LENGTH_SHORT).show();
        } else {
            try {
                createPdf(filedr, strdepsrturecity, strdestination, strdate, usermail, usermobile);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (DocumentException e) {
                e.printStackTrace();
            }
        }
       /* new SendMailTask().execute(usermail, usermobile, strdestination, strdepsrturecity);*/
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {


        if(datePickerType.equalsIgnoreCase("from")) {
            startTripDate=dayOfMonth+"/"+(monthOfYear+1)+"/"+year;
            datePickerType="to";
            datePicker();
        }else if(datePickerType.equalsIgnoreCase("to"))
        {
            endTripDate=dayOfMonth+"/"+(monthOfYear+1)+"/"+year;
            if (slctdTime == 1) {
                btnfixed.setText("From: "+startTripDate+"\nTo: "+endTripDate);
                btnflexible.setText("\nFLEXIBLE\n ");
            } else if (slctdTime == 2) {
                btnfixed.setText("\nFIXED\n ");
                btnflexible.setText("From: "+startTripDate+"\nTo: "+endTripDate);
            }
        }

    }

    private class SendMailTask extends AsyncTask<String, String, String> {

        ProgressDialog maildialog;
        String strusermail, strusermobile, strdestination, strdepsrturecity, strdate;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            maildialog = new ProgressDialog(EnquiryActivity.this);
            maildialog.setMessage("Sending Enquire...");
            maildialog.setCancelable(false);
            maildialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            strusermail = params[0];
            strusermobile = params[1];
            strdestination = params[2];
            strdepsrturecity = params[3];

            try {
                String messageTouser = "Thank you for showing intrest." + "\n" + "Our experts agents will get back to you with quotes in 24-48 hours." + "\n";

                //1st parameter Mail ID, 2nd parameter email password
                GMailSender sender = new GMailSender("itwtravelgo@gmail.com", "itwtravelgo@123");
                // sender.sendMail(params[0], params[1], "shrikantkorigeri@gmail.com", "skshreek2@gmail.com");
                sender.addAttachment(Environment.getExternalStorageDirectory().getAbsolutePath() + "/ITW/" + "ITWENQUIRE_01" + ".pdf", "ITWENQUIRE_01" + ".pdf");
                sender.sendMail("Enquire", messageTouser, "itwtravelgo@gmail.com", strusermail + ",shrikantkorigeri@gmail.com");

            } catch (Exception e) {
                Log.e("SendMail", e.getMessage(), e);
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {

            maildialog.dismiss();
            Toast.makeText(EnquiryActivity.this, "Mail sent....", Toast.LENGTH_SHORT).show();

        }
    }

    private void createPdf(String filedir, String departure, String destination, String slctddate, String stremail, String strmobilenumber) throws IOException, DocumentException {
        Document document = new Document();
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(filedir));

        MyFooter event = new MyFooter();
        writer.setPageEvent(event);
        document.open();

        Paragraph p = null, p1, p2, p4;

        Image imagelogo = null;
        try {
            Bitmap bmp = Util.getCompanyLogo(this);

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 20, stream);
            imagelogo = Image.getInstance(stream.toByteArray());
            // document.add(image);
        } catch (IOException ex) {
            return;
        }

        BaseColor myColor = WebColors.getRGBColor("#373747");
        Font fontcomapny = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD, BaseColor.WHITE);
        float[] columnWidthsimage = {1, 5};
        PdfPTable tableimagehead = new PdfPTable(columnWidthsimage);
        tableimagehead.setSpacingBefore(20f);
        tableimagehead.setSpacingAfter(20f);
        tableimagehead.setWidthPercentage(100);

        PdfPCell imagecell = new PdfPCell();
        imagelogo.setWidthPercentage(50);
        imagecell.setBorder(Rectangle.NO_BORDER);
        imagecell.addElement(imagelogo);
        imagecell.setBackgroundColor(myColor);
        imagecell.setHorizontalAlignment(Element.ALIGN_CENTER);
        imagecell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        tableimagehead.addCell(imagecell);

        PdfPCell cellcmpnyname = new PdfPCell(new Phrase(Util.cmpname, fontcomapny));
        cellcmpnyname.setBorder(Rectangle.NO_BORDER);
        cellcmpnyname.setBackgroundColor(myColor);
        cellcmpnyname.setHorizontalAlignment(Element.ALIGN_LEFT);
        cellcmpnyname.setVerticalAlignment(Element.ALIGN_MIDDLE);
        tableimagehead.addCell(cellcmpnyname);
        tableimagehead.setHorizontalAlignment(Element.ALIGN_CENTER);

        document.add(tableimagehead);

        try {
            p = new Paragraph(Util.convertDate(new Date(), "MMM dd, yyyy"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        p.setAlignment(Element.ALIGN_RIGHT);
        document.add(p);

        PdfPTable tablen = new PdfPTable(2);
        tablen.setSpacingBefore(5f);
        tablen.setSpacingAfter(2f);
        tablen.setWidthPercentage(100);

        float[] columnWidths = {1, 6, 3};
        PdfPTable table2 = new PdfPTable(columnWidths);
        table2.setHorizontalAlignment(Element.ALIGN_CENTER);
        table2.setSpacingBefore(20f);
        table2.setSpacingAfter(20f);

        Font font = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.BLACK);

        PdfPCell c1 = new PdfPCell(new Phrase("SL.NO.", font));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
        c1.setFixedHeight(35f);
        table2.addCell(c1);

        c1 = new PdfPCell(new Phrase("DESCRIPTION", font));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
        table2.addCell(c1);

        c1 = new PdfPCell(new Phrase("INTREST", font));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
        table2.addCell(c1);
        table2.setHeaderRows(1);

        Font rowfont = new Font(Font.FontFamily.HELVETICA, 9, Font.NORMAL, BaseColor.BLACK);

        //First Row
        PdfPCell frstCell = new PdfPCell(new Phrase("01", rowfont));
        frstCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        frstCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        frstCell.setFixedHeight(25f);
        table2.addCell(frstCell);

        frstCell = new PdfPCell(new Phrase("Departure City", rowfont));
        frstCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        frstCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        table2.addCell(frstCell);

        frstCell = new PdfPCell(new Phrase(String.format(departure), rowfont));
        frstCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        frstCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        table2.addCell(frstCell);

        //Second Row
        PdfPCell scndCell = new PdfPCell(new Phrase("02", rowfont));
        scndCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        scndCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        scndCell.setFixedHeight(25f);
        table2.addCell(scndCell);

        scndCell = new PdfPCell(new Phrase("Destination ", rowfont));
        scndCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        scndCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        table2.addCell(scndCell);

        scndCell = new PdfPCell(new Phrase(destination, rowfont));
        scndCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        frstCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        table2.addCell(scndCell);

        //Third Row
        PdfPCell frthCell = new PdfPCell(new Phrase("03", rowfont));
        frthCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        frthCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        frthCell.setFixedHeight(25f);
        table2.addCell(frthCell);

        frthCell = new PdfPCell(new Phrase("Selected Date ", rowfont));
        frthCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        frthCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        table2.addCell(frthCell);

        frthCell = new PdfPCell(new Phrase(slctddate, rowfont));
        frthCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        frthCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        table2.addCell(frthCell);

        //Fourth row
        PdfPCell fifthCell = new PdfPCell(new Phrase("04", rowfont));
        fifthCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        fifthCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        fifthCell.setFixedHeight(25f);
        table2.addCell(fifthCell);

        fifthCell = new PdfPCell(new Phrase("Customer Email ", rowfont));
        fifthCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        fifthCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        table2.addCell(fifthCell);

        fifthCell = new PdfPCell(new Phrase(stremail, rowfont));
        fifthCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        fifthCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        table2.addCell(fifthCell);


        //Fifth row
        PdfPCell sixthcell = new PdfPCell(new Phrase("05", rowfont));
        sixthcell.setHorizontalAlignment(Element.ALIGN_CENTER);
        sixthcell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        sixthcell.setFixedHeight(25f);
        table2.addCell(sixthcell);

        sixthcell = new PdfPCell(new Phrase("Customer Mobile ", rowfont));
        sixthcell.setHorizontalAlignment(Element.ALIGN_CENTER);
        sixthcell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        table2.addCell(sixthcell);

        sixthcell = new PdfPCell(new Phrase(strmobilenumber, rowfont));
        sixthcell.setHorizontalAlignment(Element.ALIGN_CENTER);
        sixthcell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        table2.addCell(fifthCell);


        document.add(table2);
        // document.add(table);

        Font ffont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL);

        String ws = "For, " + "\n" + Util.cmpname;
        p1 = new Paragraph(String.format("%s", ws), ffont);
        p1.setIndentationLeft(5);
        p1.setIndentationRight(50);
        p1.setSpacingBefore(20f);
        p1.setSpacingAfter(10f);


        document.add(p1);
        document.close();

        /*SendMailTask sendMailTask = new SendMailTask();
        sendMailTask.execute(new String[]{"Rent Statement for the Month of " + strdate, String.valueOf(intrpt), String.valueOf(maxrdid)});*/

        new SendMailTask().execute(stremail, strmobilenumber, destination, departure);

    }

    public class MyFooter extends PdfPageEventHelper {

        Font ffont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL);

        public void onEndPage(PdfWriter writer, Document document) {
            PdfContentByte cb = writer.getDirectContent();
            Phrase header = new Phrase("Acknowledgement", ffont);
            Phrase footer = new Phrase(Util.cmpname, ffont);
            ColumnText.showTextAligned(cb, Element.ALIGN_CENTER, header,
                    (document.right() - document.left()) / 2 + document.leftMargin(), document.top() + 10, 0);
            ColumnText.showTextAligned(cb, Element.ALIGN_CENTER, footer,
                    (document.right() - document.left()) / 2 + document.leftMargin(), document.bottom() - 10, 0);
        }
    }

    private void flexibleselected() {
        slctdTime = 2;
        datePickerType="from";
       datePicker();


        btnflexible.setBackgroundResource(R.drawable.button_with_solid_color_onrelease_orange);
        btnfixed.setBackgroundResource(R.drawable.button_with_solid_color_onrelease_purple);
        btnanytime.setBackgroundResource(R.drawable.button_with_solid_color_onrelease_purple);
    }

    private void fixedselected() {
        slctdTime = 1;
        datePickerType="from";
       datePicker();

        btnfixed.setBackgroundResource(R.drawable.button_with_solid_color_onrelease_orange);
        btnflexible.setBackgroundResource(R.drawable.button_with_solid_color_onrelease_purple);
        btnanytime.setBackgroundResource(R.drawable.button_with_solid_color_onrelease_purple);
    }

    private void anytimeSelected() {
        slctdTime = 3;
        btnfixed.setText("\nFIXED\n ");
        btnflexible.setText("\nFLEXIBLE\n ");
        btnanytime.setBackgroundResource(R.drawable.button_with_solid_color_onrelease_orange);
        btnfixed.setBackgroundResource(R.drawable.button_with_solid_color_onrelease_purple);
        btnflexible.setBackgroundResource(R.drawable.button_with_solid_color_onrelease_purple);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                Intent dashintent = new Intent(EnquiryActivity.this, Dashboard.class);
                dashintent.putExtra("from", "enquire");
                startActivity(dashintent);
                finish();// close this activity and return to preview activity (if there is any)
                break;

        }

        return super.onOptionsItemSelected(item);
    }


    public void datePicker()
    {

        if(dpd!=null)
        {
            dpd.dismiss();
            dpd=null;
        }
            Calendar now = Calendar.getInstance();
            if (dpd == null) {
                dpd = DatePickerDialog.newInstance(
                        EnquiryActivity.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
            } else {
                dpd.initialize(
                        EnquiryActivity.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
            }
        Calendar calendar = Calendar.getInstance();
        if (datePickerType.equalsIgnoreCase("from")) {
            calendar.setTime(new Date());
        }else
        {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            try {
                Date date = sdf.parse(startTripDate);
               calendar.setTime(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }




            if (datePickerType.equalsIgnoreCase("from")) {
                dpd.setMinDate(calendar);
                dpd.setTitle("From Date");
            } else {


                dpd.setMinDate(calendar);
                dpd.setTitle("To Date");

            }
            dpd.show(getFragmentManager(), "Datepickerdialog");

    }

    public static Calendar toCalendar(Date date){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }
}
