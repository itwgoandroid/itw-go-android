package com.j2ptechnologies.itwtravelgo.Activity;

import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.ColorRes;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.j2ptechnologies.itwtravelgo.R;
import com.j2ptechnologies.itwtravelgo.Utils.CommonMethodsFragments;
import com.j2ptechnologies.itwtravelgo.Utils.MaterialSearchView;
import com.j2ptechnologies.itwtravelgo.Utils.SlidingRootNav;
import com.j2ptechnologies.itwtravelgo.Utils.SlidingRootNavBuilder;
import com.j2ptechnologies.itwtravelgo.db.DatabaseHandler;
import com.j2ptechnologies.itwtravelgo.fragment.DashBoardFragment;
import com.j2ptechnologies.itwtravelgo.fragment.EnquireFormFragment;
import com.j2ptechnologies.itwtravelgo.fragment.MainScreenFragment;
import com.j2ptechnologies.itwtravelgo.fragment.NotificationFragment;
import com.j2ptechnologies.itwtravelgo.fragment.QuotationFragment;
import com.j2ptechnologies.itwtravelgo.menu.DrawerAdapter;
import com.j2ptechnologies.itwtravelgo.menu.DrawerItem;
import com.j2ptechnologies.itwtravelgo.menu.SimpleItem;
import com.j2ptechnologies.itwtravelgo.menu.SpaceItem;
import com.j2ptechnologies.itwtravelgo.util.Util;
import com.j2ptechnologies.itwtravelgo.view.LoginActivity;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Timer;


public class Dashboard extends AppCompatActivity implements DrawerAdapter.OnItemSelectedListener,
        DashBoardFragment.OnFragmentInteractionListener, NotificationFragment.OnFragmentInteractionListener,
        QuotationFragment.OnFragmentInteractionListener, MainScreenFragment.OnFragmentInteractionListener,
        EnquireFormFragment.OnFragmentInteractionListener {


    private static final int POS_DASHBOARD = 1;
    private static final int MAIN_SCREEN = 0;
    private static final int POS_ACCOUNT = 2;
    private static final int POS_MESSAGES = 3;
    private static final int QUOTATION = 4;
    private static final int LOGOUT = 7;
    private static final int ENQUIRE = 5;
    DatabaseHandler db;
    static Timer swipeTimer;
    ImageView one;
    public static TextView userName, userEmail;
    private String[] screenTitles;
    private Drawable[] screenIcons;

    private SlidingRootNav slidingRootNav;

    //private static ViewPager mPager;
    private static int currentPage = 0;
    int[] sampleImages = {R.drawable.one_n, R.drawable.two_n, R.drawable.three_n};
    private ArrayList<Integer> sliderArray = new ArrayList<Integer>();
    MaterialSearchView searchView;
    String strfrom = "";
    CarouselView carouselView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN | WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        enabledFullScreenMode();
        setContentView(R.layout.activity_dashboard);
        Util.getSoftButtonsBarSizePort(Dashboard.this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        db = new DatabaseHandler(getBaseContext());

        strfrom = getIntent().getStringExtra("from");

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_menu_button_of_three_horizontal_lines);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        searchView = (MaterialSearchView) findViewById(R.id.search_view);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }

        carouselView = (CarouselView) findViewById(R.id.carouselView);
        carouselView.setPageCount(sampleImages.length);

        carouselView.setImageListener(imageListener);

        //init();

        slidingRootNav = new SlidingRootNavBuilder(this)
                .withToolbarMenuToggle(toolbar)
                .withMenuOpened(false)
                .withContentClickableWhenMenuOpened(false)
                .withSavedState(savedInstanceState)
                .withMenuLayout(R.layout.menu_left_drawer)
                .inject();

        screenIcons = loadScreenIcons();
        screenTitles = loadScreenTitles();


        userName = (TextView) findViewById(R.id.userName);
        userEmail = (TextView) findViewById(R.id.userEmail);

        DrawerAdapter adapter = new DrawerAdapter(Arrays.asList(
                createItemFor(MAIN_SCREEN).setChecked(true),
                createItemFor(POS_DASHBOARD),
                createItemFor(POS_ACCOUNT),
                createItemFor(POS_MESSAGES),
                createItemFor(QUOTATION),
                createItemFor(ENQUIRE),
                new SpaceItem(10),
                createItemFor(LOGOUT)));
        adapter.setListener(this);

        RecyclerView list = findViewById(R.id.list);
        list.setNestedScrollingEnabled(false);
        list.setLayoutManager(new LinearLayoutManager(this));
        list.setAdapter(adapter);

        if (strfrom.equalsIgnoreCase("notification")) {
            adapter.setSelected(POS_MESSAGES);
        } else {
            MainScreenFragment fragment = new MainScreenFragment();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.container, fragment);
            transaction.commit();
        }
        //adapter.setSelected(POS_DASHBOARD);


    }

    @Override
    protected void onDestroy() {

        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        /*MenuItem item = menu.findItem(R.id.action_search);
        searchView.setMenuItem(item);*/

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_notification) {
            NotificationFragment fragment = new NotificationFragment();
            //showFragment(fragment);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.container, fragment);
            transaction.commit();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


  /*  private void init() {
        for (int i = 0; i < sliderImages.length; i++)
            sliderArray.add(sliderImages[i]);

        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(new ImageSliderAdapter(Dashboard.this, sliderArray));
        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);
        indicator.setViewPager(mPager);

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == sliderImages.length) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
        if(swipeTimer!=null)
        {
            swipeTimer.cancel();
            swipeTimer.purge();
        }
        swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3500, 4000);


    }*/

    @Override
    public void onItemSelected(int position) {
        slidingRootNav.closeMenu();

        if (position == MAIN_SCREEN) {

            MainScreenFragment fragment = new MainScreenFragment();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.container, fragment);
            transaction.addToBackStack("back");
            transaction.commit();
        } else if (position == POS_DASHBOARD) {

            DashBoardFragment fragment = new DashBoardFragment();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction().addToBackStack("");
            transaction.replace(R.id.container, fragment);
            transaction.addToBackStack("back");
            transaction.commit();
        } else if (position == POS_ACCOUNT) {
            Intent i = new Intent(getBaseContext(), AboutActivity.class);
            startActivity(i);
        } else if (position == POS_MESSAGES) {
            NotificationFragment fragment = new NotificationFragment();
            //showFragment(fragment);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction().addToBackStack("");
            transaction.replace(R.id.container, fragment);
            transaction.addToBackStack("back");
            transaction.commit();
        } else if (position == QUOTATION) {
            QuotationFragment fragment = new QuotationFragment();
            //showFragment(fragment);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction().addToBackStack("");
            transaction.replace(R.id.container, fragment);
            transaction.addToBackStack("back");
            transaction.commit();
        } else if (position == ENQUIRE) {
            /*EnquireFormFragment enqfragment = new EnquireFormFragment();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction().addToBackStack("");
            transaction.replace(R.id.container, enqfragment);
            transaction.addToBackStack("back");
            transaction.commit();*/
            Intent enqintetn = new Intent(Dashboard.this, EnquiryActivity.class);
            startActivity(enqintetn);
            this.finish();

        } else if (position == LOGOUT) {
            DatabaseHandler db = new DatabaseHandler(getBaseContext());
            Intent i = new Intent(getBaseContext(), LoginActivity.class);
            CommonMethodsFragments.addDetails("isRegistered", "false", db);
            finish();
            startActivity(i);
        }

    }

    /*private void showFragment(Fragment fragment) {
        getFragmentManager().beginTransaction()
                .replace(R.id.container, fragment)
                .commit();
    }*/

    private DrawerItem createItemFor(int position) {
        return new SimpleItem(screenIcons[position], screenTitles[position])
                .withIconTint(color(R.color.textColorSecondary))
                .withTextTint(color(R.color.textColorPrimary))
                .withSelectedIconTint(color(R.color.colorAccent))
                .withSelectedTextTint(color(R.color.colorAccent));
    }

    private String[] loadScreenTitles() {
        return getResources().getStringArray(R.array.ld_activityScreenTitles);
    }

    private Drawable[] loadScreenIcons() {
        TypedArray ta = getResources().obtainTypedArray(R.array.ld_activityScreenIcons);
        Drawable[] icons = new Drawable[ta.length()];
        for (int i = 0; i < ta.length(); i++) {
            int id = ta.getResourceId(i, 0);
            if (id != 0) {
                icons[i] = ContextCompat.getDrawable(this, id);
            }
        }
        ta.recycle();
        return icons;
    }

    private int color(@ColorRes int res) {
        return ContextCompat.getColor(this, res);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }


    public static void setUserDetails(String name, String email) {
        userName.setText(name);
        userEmail.setText(email);
    }

    @Override
    public void onBackPressed() {

        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.container);
        if (currentFragment instanceof MainScreenFragment) {

        } else {
            super.onBackPressed();
        }
    }

    ImageListener imageListener = new ImageListener() {
        @Override
        public void setImageForPosition(int position, ImageView imageView) {
            imageView.setImageResource(sampleImages[position]);
        }
    };

    private void enabledFullScreenMode() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        View decorView = getWindow().getDecorView();
        // Hide the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
    }

}
