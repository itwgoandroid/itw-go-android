package com.j2ptechnologies.itwtravelgo.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.j2ptechnologies.itwtravelgo.R;
import com.j2ptechnologies.itwtravelgo.Utils.CommonMethodsFragments;
import com.j2ptechnologies.itwtravelgo.util.Util;

import org.json.JSONException;
import org.json.JSONObject;

public class AccomodationDetailsActivity extends AppCompatActivity {

    MapView mMapView;
    private GoogleMap googleMap;
    String lattitude, longitude;
    String hotelName;
    public CardView card_view;
    public TextView date;
    public TextView title;
    public TextView status;
    public TextView checkInDate;
    public TextView checkInTime;
    public TextView checkOutDate;
    public TextView checkOutTime;
    public TextView roomNumber;
    public TextView bookingNumber;

    String jsonObject;
    String userObject;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        setContentView(R.layout.activity_accomodation_details);
        Util.getSoftButtonsBarSizePort(AccomodationDetailsActivity.this);
        try {
            Bundle bundle = getIntent().getExtras();

            lattitude = bundle.getString("lattitude");
            longitude = bundle.getString("longitude");
            hotelName = bundle.getString("hotelName");
            jsonObject = bundle.getString("jsonObject");
            userObject = bundle.getString("userObject");
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(Html.fromHtml("<small>Accomodation Details</small>"));

        card_view = (CardView) findViewById(R.id.card_view);
        // date = (TextView) findViewById(R.id.date);
        title = (TextView) findViewById(R.id.title);
        status = (TextView) findViewById(R.id.status);
        checkInDate = (TextView) findViewById(R.id.checkInDate);
        checkInTime = (TextView) findViewById(R.id.checkInTime);
        checkOutDate = (TextView) findViewById(R.id.checkOutDate);
        checkOutTime = (TextView) findViewById(R.id.checkOutTime);
        roomNumber = (TextView) findViewById(R.id.roomNumber);
        bookingNumber = (TextView) findViewById(R.id.bookingNumber);

        JSONObject jsonObjectNew = null;
        JSONObject userObjectNew = null;
        try {
            CommonMethodsFragments commonMethodsFragments = new CommonMethodsFragments();
            jsonObjectNew = new JSONObject(jsonObject);
            userObjectNew = new JSONObject(userObject);
//            date.setText(jsonObjectNew.getString("checkIn"));
            title.setText(jsonObjectNew.getString("hotelName"));
            status.setText(jsonObjectNew.getString("reservationStatus"));
            checkInDate.setText(commonMethodsFragments.changeDate(jsonObjectNew.getString("checkIn")));
            checkInTime.setText(commonMethodsFragments.changeDateToTime(jsonObjectNew.getString("checkIn")));
            checkOutDate.setText(commonMethodsFragments.changeDate(jsonObjectNew.getString("checkOut")));
            checkOutTime.setText(commonMethodsFragments.changeDateToTime(jsonObjectNew.getString("checkOut")));
            roomNumber.setText("ROOM #: " + userObjectNew.getString("roomNo"));
            bookingNumber.setText("BOOKING #: " + jsonObjectNew.getString("bookingNo"));
        } catch (JSONException e) {
            e.printStackTrace();
        }


        mMapView = (MapView) findViewById(R.id.mapView);

        mMapView.onCreate(savedInstanceState);


        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;

                // For showing a move to my location button
                //googleMap.setMyLocationEnabled(true);


                googleMap.getUiSettings().setZoomControlsEnabled(true);
                googleMap.getUiSettings().setRotateGesturesEnabled(false);
                googleMap.getUiSettings().setScrollGesturesEnabled(false);
                googleMap.getUiSettings().setTiltGesturesEnabled(false);

                // For dropping a marker at a point on the Map
                LatLng sydney = null;
                try {
                    sydney = new LatLng(Double.parseDouble(lattitude), Double.parseDouble(longitude));
                    googleMap.addMarker(new MarkerOptions().position(sydney).title(hotelName));
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }

                // For zooming automatically to the location of the marker
                CameraPosition cameraPosition = new CameraPosition.Builder().target(sydney).zoom(15).build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            Intent dashintent = new Intent(AccomodationDetailsActivity.this, Dashboard.class);
            dashintent.putExtra("from", "accomodation");
            startActivity(dashintent);
            finish();// close this activity and return to preview activity (if there is any)
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
