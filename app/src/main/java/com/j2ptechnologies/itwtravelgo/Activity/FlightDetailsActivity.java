package com.j2ptechnologies.itwtravelgo.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.j2ptechnologies.itwtravelgo.R;
import com.j2ptechnologies.itwtravelgo.Utils.CommonMethodsFragments;
import com.j2ptechnologies.itwtravelgo.db.IataHelper;
import com.j2ptechnologies.itwtravelgo.db.IataModel;
import com.j2ptechnologies.itwtravelgo.util.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Set;

public class FlightDetailsActivity extends AppCompatActivity {

    MapView mMapView;
    private GoogleMap googleMap;
    double lat, lng;
    TextView name, city, country;
    public TextView from;
    public TextView to;
    public TextView departureDate;
    public TextView departureTime;
    public TextView departureFrom;
    public TextView arrivalDate;
    public TextView arrivalTime;
    public TextView arrivalTo;
    public TextView bookingNo;
    public TextView date;
    public TextView type;
    public TextView seatNo;
    public TextView meals;
    public TextView flightClass;
    String details;
    String userObject;
    String flightType, fromLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        setContentView(R.layout.activity_map);
        Util.getSoftButtonsBarSizePort(FlightDetailsActivity.this);
        try {
            Bundle bundle = getIntent().getExtras();
            fromLocation = bundle.getString("fromLocation");
            details = bundle.getString("jsonObject");
            userObject = bundle.getString("userObject");
            flightType = bundle.getString("type");
        } catch (Exception e) {
            e.printStackTrace();
        }
        getSupportActionBar().setTitle(Html.fromHtml("<small>Flight Details</small>"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        IataHelper mDbHelper = new IataHelper(getBaseContext());
        mDbHelper.createDatabase();
        mDbHelper.open();
        HashMap<String, String> updates = updateLocations();
        Set<String> keys = updates.keySet();
        for (String key : keys) {
            String[] updateLoc = updates.get(key).split(",");
            mDbHelper.updateLatLong(key, Double.parseDouble(updateLoc[0]), Double.parseDouble(updateLoc[1]));

        }
        final IataModel getIataDetails = mDbHelper.getsavedIataDetails(fromLocation);


        mDbHelper.close();

        mMapView = (MapView) findViewById(R.id.mapView);
        name = (TextView) findViewById(R.id.name);
        type = (TextView) findViewById(R.id.type);
        city = (TextView) findViewById(R.id.city);
        country = (TextView) findViewById(R.id.country);
        from = (TextView) findViewById(R.id.from);
        to = (TextView) findViewById(R.id.to);
        seatNo = (TextView) findViewById(R.id.seatNo);
        meals = (TextView) findViewById(R.id.meals);
        flightClass = (TextView) findViewById(R.id.flightClass);
        departureDate = (TextView) findViewById(R.id.departureDate);
        departureTime = (TextView) findViewById(R.id.departureTime);
        departureFrom = (TextView) findViewById(R.id.departureFrom);
        arrivalDate = (TextView) findViewById(R.id.arrivalDate);
        arrivalTime = (TextView) findViewById(R.id.arrivalTime);
        arrivalTo = (TextView) findViewById(R.id.arrivalTo);
        bookingNo = (TextView) findViewById(R.id.bookingNo);


        name.setText("Name    : " + getIataDetails.getName().substring(0, 1).toUpperCase() + getIataDetails.getName().substring(1).toLowerCase());
        city.setText("City    : " + getIataDetails.getCity().substring(0, 1).toUpperCase() + getIataDetails.getCity().substring(1).toLowerCase());
        country.setText("Country : " + getIataDetails.getCountry().substring(0, 1).toUpperCase() + getIataDetails.getCountry().substring(1).toLowerCase());
        mMapView.onCreate(savedInstanceState);

        try {
            JSONObject jsonObject = new JSONObject(details);
            from.setText(jsonObject.getString("departureAirport"));
            to.setText(jsonObject.getString("arrivalAirport"));
            CommonMethodsFragments commonMethodsFragments = new CommonMethodsFragments();
            departureDate.setText(commonMethodsFragments.changeDate(jsonObject.getString("departureDate")));
            departureTime.setText(commonMethodsFragments.changeDateToTime(jsonObject.getString("departureDate")));
            departureFrom.setText(jsonObject.getString("departureTerminal"));
            arrivalDate.setText(commonMethodsFragments.changeDate(jsonObject.getString("arrivalDate")));
            arrivalTime.setText(commonMethodsFragments.changeDateToTime(jsonObject.getString("arrivalDate")));
            arrivalTo.setText(jsonObject.getString("arrivalTerminal"));
            bookingNo.setText("BOOKING #: " + jsonObject.getString("bookingNo"));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        type.setText("Type : " + flightType);

        try {
            JSONObject jsonObject = new JSONObject(userObject);
            if (flightType.equalsIgnoreCase("onward")) {
                seatNo.setText("Seat No. : " + jsonObject.getString("onwardSeatNo"));
                meals.setText("Meals : " + jsonObject.getString("onwardMeal"));
                flightClass.setText("Class : " + jsonObject.getString("onwardClass"));
            } else {
                seatNo.setText("Seat No. : " + jsonObject.getString("returnSeatNo"));
                meals.setText("Meals : " + jsonObject.getString("returnMeal"));
                flightClass.setText("Class : " + jsonObject.getString("returnClass"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;

                googleMap.getUiSettings().setZoomControlsEnabled(true);
                googleMap.getUiSettings().setRotateGesturesEnabled(false);
                googleMap.getUiSettings().setScrollGesturesEnabled(false);
                googleMap.getUiSettings().setTiltGesturesEnabled(false);
                // For showing a move to my location button
                //googleMap.setMyLocationEnabled(true);

                // For dropping a marker at a point on the Map
                LatLng sydney = new LatLng(getIataDetails.getLattitude(), getIataDetails.getLongitude());
                googleMap.addMarker(new MarkerOptions().position(sydney).title(getIataDetails.getName()));

                // For zooming automatically to the location of the marker
                CameraPosition cameraPosition = new CameraPosition.Builder().target(sydney).zoom(15).build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            Intent dashintent = new Intent(FlightDetailsActivity.this, Dashboard.class);
            dashintent.putExtra("from", "flight");
            startActivity(dashintent);
            finish();// close this activity and return to preview activity (if there is any)
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public HashMap<String, String> updateLocations() {
        HashMap<String, String> updates = new HashMap<>();
        updates.put("BLR", "13.1986,77.7066");

        return updates;
    }
}
