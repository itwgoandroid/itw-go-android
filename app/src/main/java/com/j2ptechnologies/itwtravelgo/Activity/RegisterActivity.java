package com.j2ptechnologies.itwtravelgo.Activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.view.Gravity;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.animation.AccelerateInterpolator;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.j2ptechnologies.itwtravelgo.R;
import com.j2ptechnologies.itwtravelgo.Utils.CatLoadingView;
import com.j2ptechnologies.itwtravelgo.Utils.CommonMethods;
import com.j2ptechnologies.itwtravelgo.Utils.CommonMethodsFragments;
import com.j2ptechnologies.itwtravelgo.Utils.Constants;
import com.j2ptechnologies.itwtravelgo.Utils.LovelyInfoDialog;
import com.j2ptechnologies.itwtravelgo.api.PostApi;
import com.j2ptechnologies.itwtravelgo.app.App;
import com.j2ptechnologies.itwtravelgo.db.DatabaseHandler;
import com.j2ptechnologies.itwtravelgo.util.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Pattern;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class RegisterActivity extends CommonMethods {
    FloatingActionButton fab;
    CardView cvAdd;
    private final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
    private EditText firstName, lastName, mobile, email, password;
    Button bt_go;
    public static String STR_EXTRA_ACTION_REGISTER = "register";
    CatLoadingView mView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        setContentView(R.layout.activity_register);
        Util.getSoftButtonsBarSizePort(RegisterActivity.this);

        mView = new CatLoadingView();
        fab = (FloatingActionButton) findViewById(R.id.fab);
        cvAdd = (CardView) findViewById(R.id.cv_add);
        firstName = (EditText) findViewById(R.id.firstName);
        lastName = (EditText) findViewById(R.id.lastName);
        mobile = (EditText) findViewById(R.id.mobile);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ShowEnterAnimation();
        }
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animateRevealClose();
            }
        });


        bt_go = (Button) findViewById(R.id.bt_go);

        bt_go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (firstName.getText().toString().trim().length() == 0) {
                    firstName.setError("Please enter First Name");
                    return;
                }

                if (lastName.getText().toString().trim().length() == 0) {
                    lastName.setError("Please enter Last Name");
                    return;
                }

                if (mobile.getText().toString().trim().length() < 0) {
                    mobile.setError("Please enter valid Mobile Number");
                    return;
                }

                if (password.getText().toString().trim().length() < 6) {
                    password.setError("Password lenght must be min 6");
                    return;
                }

                if (!validateEmail((email.getText().toString().trim()))) {
                    email.setError("Please enter valid Email");
                    return;
                }

                JSONObject json = new JSONObject();

                try {
                    json.put("firstName", firstName.getText().toString().trim());
                    json.put("lastName", lastName.getText().toString().trim());
                    json.put("mobileNumber", mobile.getText().toString().trim());
                    json.put("email", email.getText().toString().trim());
                    json.put("password", password.getText().toString().trim());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                JSONObject users = new JSONObject();
                try {
                    users.put("users", json);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                saveUser(users, email.getText().toString().trim());


            }
        });

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void ShowEnterAnimation() {
        Transition transition = TransitionInflater.from(this).inflateTransition(R.transition.fabtransition);
        getWindow().setSharedElementEnterTransition(transition);

        transition.addListener(new Transition.TransitionListener() {
            @Override
            public void onTransitionStart(Transition transition) {
                cvAdd.setVisibility(View.GONE);
            }

            @Override
            public void onTransitionEnd(Transition transition) {
                transition.removeListener(this);
                animateRevealShow();
            }

            @Override
            public void onTransitionCancel(Transition transition) {

            }

            @Override
            public void onTransitionPause(Transition transition) {

            }

            @Override
            public void onTransitionResume(Transition transition) {

            }


        });
    }

    public void animateRevealShow() {
        Animator mAnimator = ViewAnimationUtils.createCircularReveal(cvAdd, cvAdd.getWidth() / 2, 0, fab.getWidth() / 2, cvAdd.getHeight());
        mAnimator.setDuration(500);
        mAnimator.setInterpolator(new AccelerateInterpolator());
        mAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
            }

            @Override
            public void onAnimationStart(Animator animation) {
                cvAdd.setVisibility(View.VISIBLE);
                super.onAnimationStart(animation);
            }
        });
        mAnimator.start();
    }


    public void animateRevealClose() {
        Animator mAnimator = ViewAnimationUtils.createCircularReveal(cvAdd, cvAdd.getWidth() / 2, 0, cvAdd.getHeight(), fab.getWidth() / 2);
        mAnimator.setDuration(500);
        mAnimator.setInterpolator(new AccelerateInterpolator());
        mAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                cvAdd.setVisibility(View.INVISIBLE);
                super.onAnimationEnd(animation);
                fab.setImageResource(R.drawable.ic_signup);
                RegisterActivity.super.onBackPressed();
            }

            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
            }
        });
        mAnimator.start();
    }

    @Override
    public void onBackPressed() {
        animateRevealClose();
    }


    public void saveUser(JSONObject json, final String email) {
        String URL = Constants.saveUser;
        showDialog();

        PostApi api = new PostApi(Request.Method.POST, URL, json, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                dismissDialog();
                try {
                    int code = jsonObject.getInt("code");
                    if (code == 200) {
                        DatabaseHandler db = new DatabaseHandler(getBaseContext());
                        CommonMethodsFragments.addDetails("currentEmail", email, db);
                        CommonMethodsFragments.addDetails("isRegistered", "true", db);

                        Intent i = new Intent(getBaseContext(), Dashboard.class);
                        i.putExtra("from", "register");
                        startActivity(i);
                    } else if (code == 500) {
                        DatabaseHandler db = new DatabaseHandler(getBaseContext());
                        Intent i = new Intent(getBaseContext(), ResetPassword.class);
                        i.putExtra("currentEmail", email);
                        startActivity(i);
                    } else {
                        new LovelyInfoDialog(getBaseContext())
                                .setTopColorRes(R.color.colorAccent)
                                .setIcon(R.drawable.ic_tab_infor)

                                //This will add Don't show again checkbox to the dialog. You can pass any ID as argument
                                .setTitleGravity(Gravity.CENTER)
                                .setNotShowAgainOptionChecked(false)
                                .setTitle("Alert!")

                                .setMessage("This user already exist. Please Register as different user")
                                .show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Snackbar snackbar = null;
                dismissDialog();
                final Snackbar finalSnackbar = snackbar;
                snackbar = Snackbar
                        .make(getWindow().getDecorView(), "Something went wrong", Snackbar.LENGTH_LONG)
                        .setAction("", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                            }
                        });

                snackbar.show();


            }
        });

        App.getVolleyQueue().add(api);
    }

    public void showDialog() {
        mView.setClickCancelAble(false);
        mView.show(getSupportFragmentManager(), "");
    }

    public void dismissDialog() {
        mView.dismiss();
    }
}
