package com.j2ptechnologies.itwtravelgo.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;

import com.j2ptechnologies.itwtravelgo.R;
import com.j2ptechnologies.itwtravelgo.Utils.CommonMethodsFragments;
import com.j2ptechnologies.itwtravelgo.calendar.CustomCalendar;
import com.j2ptechnologies.itwtravelgo.calendar.dao.EventData;
import com.j2ptechnologies.itwtravelgo.calendar.dao.dataAboutDate;
import com.j2ptechnologies.itwtravelgo.db.DatabaseHandler;
import com.j2ptechnologies.itwtravelgo.util.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import static com.j2ptechnologies.itwtravelgo.Utils.CommonMethodsFragments.getDetails;

public class CalendarViewActivity extends AppCompatActivity {

    public CustomCalendar customCalendars;
    ArrayList<String> arr = new ArrayList<String>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_calander);
        Util.getSoftButtonsBarSizePort(CalendarViewActivity.this);
        customCalendars = (CustomCalendar) findViewById(R.id.customCalendar);
        DatabaseHandler db = new DatabaseHandler(getBaseContext());
        HashMap<String, ArrayList<EventData>> checklist = loadItinerary(CommonMethodsFragments.getDetails("completeDetails", db), db);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(Html.fromHtml("<small>View On Calendar</small>"));

        TreeMap<String, ArrayList<EventData>> sorted = new TreeMap<>();
        sorted.putAll(checklist);

        // Display the TreeMap which is naturally sorted
        for (Map.Entry<String, ArrayList<EventData>> entry : sorted.entrySet()) {
            System.out.println("Key = " + entry.getKey());
            Log.e("BEFORE", "" + entry.getKey());
            customCalendars.addAnEvent(entry.getKey(), 3, checklist.get(entry.getKey()));
        }


      /*  for (String key : checklist.keySet()) {
            customCalendars.addAnEvent(key, 3, checklist.get(key));
            Log.e("CALENDER", key);
        }*/


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            /*Intent dashintent = new Intent(AboutActivity.this, Dashboard.class);
            dashintent.putExtra("from", "accomodation");
            startActivity(dashintent);*/
            finish();// close this activity and return to preview activity (if there is any)
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public HashMap<String, ArrayList<EventData>> loadItinerary(String jsn, DatabaseHandler db) {
        HashMap<String, ArrayList<EventData>> checklist = new HashMap<>();


        JSONObject jsonNew = null;
        try {
            jsonNew = new JSONObject(jsn);
            JSONObject dat = jsonNew.getJSONObject("data");
            JSONObject users = dat.getJSONObject("users");
            JSONArray trips = users.getJSONArray("trips");
            for (int m = 0; m < trips.length(); m++) {

                JSONObject json = trips.getJSONObject(m);
                if (json.getString("id").equals(getDetails("currentTripId", db))) {
                    // ArrayList<EventData> eventDataList = new ArrayList();
                    try {
                        JSONArray flightDetails = json.getJSONArray("flights");
                        for (int i = 0; i < flightDetails.length(); i++) {
                            ArrayList<EventData> eventDataList = new ArrayList();
                            JSONObject flightObject = flightDetails.getJSONObject(i);
                            String eventDate = CommonMethodsFragments.changeDateTimeToDate(flightObject.getString("departureDate"));
                            String eventTitle = flightObject.getString("type") + " " + "Flight";
                            eventTitle = eventTitle.toUpperCase();

                            String title = "Airport : " + flightObject.getString("departureAirport") + ",   " + CommonMethodsFragments.changeDateToTimeNew(flightObject.getString("departureDate"));
                            String subTitle = "Flight # : " + flightObject.getString("flightNo");
                            String subject = "Airline : " + flightObject.getString("airline");
                            ArrayList<dataAboutDate> dataAboutDates = new ArrayList();
                            dataAboutDate dataAboutDate = new dataAboutDate();
                            dataAboutDate.setTitle(title);
                            dataAboutDate.setSubject(subTitle);
                            dataAboutDate.setRemarks(subject);
                            dataAboutDate.setSubmissionDate(" ");
                            // arr.clear();
                            //arr.add(eventDate);
                            dataAboutDates.add(dataAboutDate);

                            EventData eventData = new EventData();
                            eventData.setSection(eventTitle);
                            eventData.setData(dataAboutDates);
                            eventDataList.add(eventData);
                            //eventDataList.
                            if (checklist.containsKey(eventDate)) {
                                ArrayList<EventData> eventDataListCheck = checklist.get(eventDate);
                                eventDataListCheck.add(eventData);
                                checklist.put(eventDate, eventDataListCheck);
                            } else {
                                checklist.put(eventDate, eventDataList);


                            }
                            //customCalendars.addAnEvent(eventDate, 3, eventDataList);

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    try {
                        JSONArray accomodationDetails = json.getJSONArray("accomodations");
                        for (int i = 0; i < accomodationDetails.length(); i++) {

                            ArrayList<EventData> eventDataList = new ArrayList();
                            JSONObject accomodationObject = accomodationDetails.getJSONObject(i);
                            String eventDate = CommonMethodsFragments.changeDateTimeToDate(accomodationObject.getString("checkIn"));
                            String eventTitle = "Accomodation".toUpperCase();
                            String title = "Hotel : " + accomodationObject.getString("hotelName");
                            String subTitle = "Time : " + CommonMethodsFragments.changeDateToTimeNew(accomodationObject.getString("checkIn"));
                            String subject = "Status : " + accomodationObject.getString("reservationStatus");
                            ArrayList<dataAboutDate> dataAboutDates = new ArrayList();
                            dataAboutDate dataAboutDate = new dataAboutDate();
                            dataAboutDate.setTitle(title);
                            dataAboutDate.setSubject(subTitle);
                            dataAboutDate.setRemarks(subject);
                            dataAboutDate.setSubmissionDate(" ");
                            // arr.clear();
                            //arr.add(eventDate);
                            dataAboutDates.add(dataAboutDate);
                            EventData eventData = new EventData();
                            eventData.setSection(eventTitle);
                            eventData.setData(dataAboutDates);
                            eventDataList.add(eventData);
                            if (checklist.containsKey(eventDate)) {
                                ArrayList<EventData> eventDataListCheck = checklist.get(eventDate);
                                eventDataListCheck.add(eventData);
                                checklist.put(eventDate, eventDataListCheck);
                            } else {
                                checklist.put(eventDate, eventDataList);
                            }
                            //customCalendars.addAnEvent(eventDate, 3, eventDataList);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    try {
                        JSONArray tripsDetails = json.getJSONArray("transports");
                        for (int i = 0; i < tripsDetails.length(); i++) {


                            ArrayList<EventData> eventDataList = new ArrayList();
                            JSONObject transportationObject = tripsDetails.getJSONObject(i);
                            String eventDate = CommonMethodsFragments.changeDateTimeToDate(transportationObject.getString("startDate"));
                            String eventTitle = "Transportation".toUpperCase();
                            String title = transportationObject.getString("name") + "  " + transportationObject.getString("transportTitle");
                            String subTitle = "Pickup : " + transportationObject.getString("pickupLocation") + ",   " + CommonMethodsFragments.changeDateToTimeNew(transportationObject.getString("startDate"));
                            String subject = "Contact : " + transportationObject.getString("contact");
                            ArrayList<dataAboutDate> dataAboutDates = new ArrayList();
                            dataAboutDate dataAboutDate = new dataAboutDate();
                            dataAboutDate.setTitle(title);
                            dataAboutDate.setSubject(subTitle);
                            dataAboutDate.setRemarks(subject);
                            dataAboutDate.setSubmissionDate(" ");
                            // arr.clear();
                            //arr.add(eventDate);
                            dataAboutDates.add(dataAboutDate);
                            EventData eventData = new EventData();
                            eventData.setSection(eventTitle);
                            eventData.setData(dataAboutDates);
                            eventDataList.add(eventData);
                            if (checklist.containsKey(eventDate)) {
                                ArrayList<EventData> eventDataListCheck = checklist.get(eventDate);
                                eventDataListCheck.add(eventData);
                                checklist.put(eventDate, eventDataListCheck);
                            } else {
                                checklist.put(eventDate, eventDataList);
                            }
                            //customCalendars.addAnEvent(eventDate, 3, eventDataList);

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    try {
                        JSONArray activityDetails = json.getJSONArray("activities");
                        for (int i = 0; i < activityDetails.length(); i++) {

                            ArrayList<EventData> eventDataList = new ArrayList();
                            JSONObject activityObject = activityDetails.getJSONObject(i);
                            String eventDate = CommonMethodsFragments.changeDateTimeToDate(activityObject.getString("activityDate"));
                            String eventTitle = "Activity".toUpperCase();
                            String title = activityObject.getString("activityName") + ",  " + activityObject.getString("activityDate");
                            String subTitle = "Location : " + activityObject.getString("location");
                            String subject = "Contact : " + activityObject.getString("contact");
                            ArrayList<dataAboutDate> dataAboutDates = new ArrayList();
                            dataAboutDate dataAboutDate = new dataAboutDate();
                            dataAboutDate.setTitle(title);
                            dataAboutDate.setSubject(subTitle);
                            dataAboutDate.setRemarks(subject);
                            dataAboutDate.setSubmissionDate(" ");
                            // arr.clear();
                            //arr.add(eventDate);
                            dataAboutDates.add(dataAboutDate);
                            EventData eventData = new EventData();
                            eventData.setSection(eventTitle);
                            eventData.setData(dataAboutDates);
                            eventDataList.add(eventData);
                            if (checklist.containsKey(eventDate)) {
                                ArrayList<EventData> eventDataListCheck = checklist.get(eventDate);
                                eventDataListCheck.add(eventData);
                                checklist.put(eventDate, eventDataListCheck);
                            } else {
                                checklist.put(eventDate, eventDataList);
                            }
                            //customCalendars.addAnEvent(eventDate, 3, eventDataList);


                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return checklist;
    }
}
