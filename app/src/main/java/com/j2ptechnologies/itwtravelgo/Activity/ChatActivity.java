package com.j2ptechnologies.itwtravelgo.Activity;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.j2ptechnologies.itwtravelgo.BuildConfig;
import com.j2ptechnologies.itwtravelgo.R;
import com.j2ptechnologies.itwtravelgo.Utils.CommonMethodsFragments;
import com.j2ptechnologies.itwtravelgo.adapter.ChatFirebaseAdapter;
import com.j2ptechnologies.itwtravelgo.adapter.ClickListenerChatFirebase;
import com.j2ptechnologies.itwtravelgo.db.DatabaseHandler;
import com.j2ptechnologies.itwtravelgo.model.ChatModel;
import com.j2ptechnologies.itwtravelgo.model.FileModel;
import com.j2ptechnologies.itwtravelgo.model.MapModel;
import com.j2ptechnologies.itwtravelgo.model.UserModel;
import com.j2ptechnologies.itwtravelgo.util.PrefUtils;
import com.j2ptechnologies.itwtravelgo.util.Util;
import com.j2ptechnologies.itwtravelgo.view.FullScreenImageActivity;
import com.j2ptechnologies.itwtravelgo.view.LoginActivity;
import com.j2ptechnologies.itwtravelgo.view.RotateLoading;

import java.io.File;
import java.util.Calendar;
import java.util.Date;

import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;

public class ChatActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener, ClickListenerChatFirebase {

    private static final int IMAGE_GALLERY_REQUEST = 1;
    private static final int IMAGE_CAMERA_REQUEST = 2;
    private static final int PLACE_PICKER_REQUEST = 3;
    private final static int PICK_FILE_REQUEST = 4;

    static final String TAG = ChatActivity.class.getSimpleName();
    static final String CHAT_REFERENCE = "messages";
    public static boolean isAppRunning;
    //Firebase and GoogleApiClient
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private GoogleApiClient mGoogleApiClient;
    private DatabaseReference mFirebaseDatabaseReference;
    FirebaseStorage storage = FirebaseStorage.getInstance();
    String[] array;

    //CLass Model
    private UserModel userModel;

    //Views UI
    private RecyclerView rvListMessage;
    private LinearLayoutManager mLinearLayoutManager;
    private ImageView btSendMessage, btEmoji, attach;
    private ImageView gallery, camera, location, img_pdf;
    private EmojiconEditText edMessage;
    private View contentRoot;
    private EmojIconActions emojIcon;

    //File
    private File filePathImageCamera;
    View bottomSheet;
    BottomSheetBehavior behavior;

    final DatabaseHandler db = new DatabaseHandler(getBaseContext());
    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    private RotateLoading rotateLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        setContentView(R.layout.activity_main);
        Util.getSoftButtonsBarSizePort(ChatActivity.this);

        isAppRunning=true;
        bottomSheet = findViewById(R.id.design_bottom_sheet);
        behavior = BottomSheetBehavior.from(bottomSheet);
        rotateLoading = (RotateLoading) findViewById(R.id.rotateloading);

        DatabaseHandler db=new DatabaseHandler(getBaseContext());
        CommonMethodsFragments.addDetails("messageCount","0",db);

        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(Html.fromHtml("<small>Chat with us</small>"));

        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_DRAGGING:
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_DRAGGING");
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_SETTLING");
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_EXPANDED");
                        break;
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_COLLAPSED");
                        break;
                    case BottomSheetBehavior.STATE_HIDDEN:
                        Log.i("BottomSheetCallback", "BottomSheetBehavior.STATE_HIDDEN");
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                Log.i("BottomSheetCallback", "slideOffset: " + slideOffset);
            }
        });

        if (!Util.verificaConexao(this)) {
            Util.initToast(this, "No internet conectivity");
            finish();
        } else {

            bindViews();
            // getAllusers();
            verificaUsuarioLogado();
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .enableAutoManage(this, this)
                    .addApi(Auth.GOOGLE_SIGN_IN_API)
                    .build();
        }
    }

    @Override
    protected void onRestart() {
        isAppRunning=true;
        super.onRestart();
    }

    @Override
    protected void onDestroy() {
        isAppRunning=false;
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        isAppRunning=false;
        super.onPause();
    }

    private void getAllusers() {
        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();

        DatabaseReference usersdRef = rootRef.child("users");

        ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {

                    String name = ds.child("name").getValue(String.class);

                    Log.d("TAG", name);

                    //   Toast.makeText(ChatActivity.this, " User " + name, Toast.LENGTH_SHORT).show();
                    //array.add(name);

                }
               /* ArrayAdapter<String> adapter = new ArrayAdapter(MainActivity.this, android.R.layout.simple_list_item_1, array);

                mListView.setAdapter(adapter);*/

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        usersdRef.addListenerForSingleValueEvent(eventListener);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        StorageReference storageRef = storage.getReferenceFromUrl(Util.URL_STORAGE_REFERENCE).child(Util.FOLDER_STORAGE_IMG);

        if (requestCode == IMAGE_GALLERY_REQUEST) {
            if (resultCode == RESULT_OK) {
                Uri selectedImageUri = data.getData();
                if (selectedImageUri != null) {
                    sendFileFirebase(storageRef, selectedImageUri);
                } else {
                    //URI IS NULL
                }
            }
        } else if (requestCode == IMAGE_CAMERA_REQUEST) {
            if (resultCode == RESULT_OK) {
                if (filePathImageCamera != null && filePathImageCamera.exists()) {
                    StorageReference imageCameraRef = storageRef.child(filePathImageCamera.getName() + "_camera");
                    sendFileFirebase(imageCameraRef, filePathImageCamera);
                } else {
                    //IS NULL
                }
            }
        } else if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(this, data);
                if (place != null) {
                    LatLng latLng = place.getLatLng();
                    MapModel mapModel = new MapModel(latLng.latitude + "", latLng.longitude + "");
                    ChatModel chatmodel = new ChatModel("location", latLng.latitude + "", latLng.longitude + "", userModel.getEmail(), "", "", "", "", "123", Calendar.getInstance().getTime().getTime());
                    mFirebaseDatabaseReference.child(CHAT_REFERENCE).child("Admin_" + userModel.getName()).push().setValue(chatmodel);
                    increaseCount();
                    reduceAdmincount();
                } else {
                    //PLACE IS NULL
                }
            }
        } else if (requestCode == PICK_FILE_REQUEST && resultCode == RESULT_OK) {
            Uri selectedImageUri = data.getData();
            uploadImage(storageRef, selectedImageUri);

        }

    }

    @Override
    public void onBackPressed() {
        Intent i=new Intent(getBaseContext(),Dashboard.class);
        i.putExtra("from","ChatActivity");
        startActivity(i);
        super.onBackPressed();
    }

   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_chat, menu);
        return true;
    }*/

 /*   @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                Intent dashintent = new Intent(ChatActivity.this, Dashboard.class);
                dashintent.putExtra("from", "chatactivity");
                startActivity(dashintent);
                finish();// close this activity and return to preview activity (if there is any)
                break;

            case R.id.sendPhoto:
                verifyStoragePermissions();
//                photoCameraIntent();
                break;
            case R.id.sendPhotoGallery:
                photoGalleryIntent();
                break;
            case R.id.sendLocation:
                locationPlacesIntent();
                break;
            case R.id.sign_out:
                signOut();
                break;
        }

        return super.onOptionsItemSelected(item);
    }*/

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
        Util.initToast(this, "Google Play Services error.");
    }


    @Override
    public void clickImageChat(View view, int position, String nameUser, String urlPhotoUser, String urlPhotoClick) {
        Intent intent = new Intent(this, FullScreenImageActivity.class);
        intent.putExtra("nameUser", nameUser);
        intent.putExtra("urlPhotoUser", urlPhotoUser);
        intent.putExtra("urlPhotoClick", urlPhotoClick);
        startActivity(intent);
    }

    @Override
    public void clickImageMapChat(View view, int position, String latitude, String longitude) {
        String uri = String.format("geo:%s,%s?z=17&q=%s,%s", latitude, longitude, latitude, longitude);
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        startActivity(intent);
    }


    /**
     * Envia o arvquivo para o firebase
     */
    private void sendFileFirebase(StorageReference storageReference, final Uri file) {
        if (storageReference != null) {
            final String name = DateFormat.format("yyyy-MM-dd_hhmmss", new Date()).toString();
            StorageReference imageGalleryRef = storageReference.child(name + "_gallery");

            rotateLoading.setVisibility(View.VISIBLE);
            rotateLoading.start();

            UploadTask uploadTask = imageGalleryRef.putFile(file);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.e(TAG, "onFailure sendFileFirebase " + e.getMessage());
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                    if (rotateLoading.isStart()) {
                        rotateLoading.stop();
                        rotateLoading.setVisibility(View.GONE);
                    }

                    Log.i(TAG, "onSuccess sendFileFirebase");
                    Uri downloadUrl = taskSnapshot.getDownloadUrl();
                    FileModel fileModel = new FileModel("img", downloadUrl.toString(), name, "");
                    ChatModel chatmodel = new ChatModel("media", "", "", userModel.getEmail(), "img",
                            name, downloadUrl.toString(), "", "123", Calendar.getInstance().getTime().getTime());
                    mFirebaseDatabaseReference.child(CHAT_REFERENCE).child("Admin_" + userModel.getName()).push().setValue(chatmodel);
                    increaseCount();
                    reduceAdmincount();
                }
            });
        } else {
            //IS NULL
        }

    }

    /**
     * Envia o arvquivo para o firebase
     */
    private void sendFileFirebase(StorageReference storageReference, final File file) {
        if (storageReference != null) {
            Uri photoURI = FileProvider.getUriForFile(ChatActivity.this,
                    BuildConfig.APPLICATION_ID + ".provider",
                    file);

            rotateLoading.setVisibility(View.VISIBLE);
            rotateLoading.start();

            UploadTask uploadTask = storageReference.putFile(photoURI);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.e(TAG, "onFailure sendFileFirebase " + e.getMessage());
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    if (rotateLoading.isStart()) {
                        rotateLoading.stop();
                        rotateLoading.setVisibility(View.GONE);
                    }

                    Log.i(TAG, "onSuccess sendFileFirebase");
                    Uri downloadUrl = taskSnapshot.getDownloadUrl();
                    FileModel fileModel = new FileModel("img", downloadUrl.toString(), file.getName(), file.length() + "");
                    ChatModel chatmodel = new ChatModel("media", "", "", userModel.getEmail(), "img", file.getName(), downloadUrl.toString(), "", "123", Calendar.getInstance().getTime().getTime());
                    mFirebaseDatabaseReference.child(CHAT_REFERENCE).child("Admin_" + userModel.getName()).push().setValue(chatmodel);

                    increaseCount();
                    reduceAdmincount();
                }
            });
        } else {
            //IS NULL
        }

    }

    /**
     * Obter local do usuario
     */
    private void locationPlacesIntent() {
        try {
            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
            startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    /**
     * Enviar foto tirada pela camera
     */
    private void photoCameraIntent() {
        String nomeFoto = DateFormat.format("yyyy-MM-dd_hhmmss", new Date()).toString();
        filePathImageCamera = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), nomeFoto + "camera.jpg");
        Intent it = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Uri photoURI = FileProvider.getUriForFile(ChatActivity.this,
                BuildConfig.APPLICATION_ID + ".provider",
                filePathImageCamera);
        it.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
        startActivityForResult(it, IMAGE_CAMERA_REQUEST);
    }

    /**
     * Enviar foto pela galeria
     */
    private void photoGalleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, getString(R.string.select_picture_title)), IMAGE_GALLERY_REQUEST);
    }

    /**
     * Enviar msg de texto simples para chat
     */
    private void sendMessageFirebase() {
        ChatModel chatmodel = new ChatModel("textMessage", "", "", userModel.getEmail(), "",
                "", "", edMessage.getText().toString(), "123", Calendar.getInstance().getTime().getTime());
        mFirebaseDatabaseReference.child(CHAT_REFERENCE).child("Admin_" + userModel.getName()).push().setValue(chatmodel);


        increaseCount();
        reduceAdmincount();

        /*final Firebase reference = new Firebase("https://itwtravelgo-3bb51.firebaseio.com/users");
        reference.addListenerForSingleValueEvent(new com.firebase.client.ValueEventListener() {
            @Override
            public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {
                for (com.firebase.client.DataSnapshot snap : dataSnapshot.getChildren()) {
                    if (snap.child("email").getValue().equals(CommonMethodsFragments.getDetails("email", db))) {
                        //  Toast.makeText(getBaseContext(), snap.child("count").getValue() + "", Toast.LENGTH_LONG).show();
                        Log.e(snap.getKey(), snap.getChildrenCount() + "");
                    }
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }


        });*/
        edMessage.setText(null);
    }

    /**
     * Ler collections chatmodel Firebase
     */
    private void lerMessagensFirebase() {
        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();
        final ChatFirebaseAdapter firebaseAdapter = new ChatFirebaseAdapter(ChatActivity.this, mFirebaseDatabaseReference.child(CHAT_REFERENCE).child("Admin_" + userModel.getName()), userModel.getEmail(), this);
        firebaseAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
                int friendlyMessageCount = firebaseAdapter.getItemCount();
                int lastVisiblePosition = mLinearLayoutManager.findLastCompletelyVisibleItemPosition();
                if (lastVisiblePosition == -1 ||
                        (positionStart >= (friendlyMessageCount - 1) &&
                                lastVisiblePosition == (positionStart - 1))) {
                    rvListMessage.scrollToPosition(positionStart);
                }
            }
        });
        rvListMessage.setLayoutManager(mLinearLayoutManager);
        rvListMessage.setAdapter(firebaseAdapter);
    }

    /**
     * Verificar se usuario está logado
     */
    private void verificaUsuarioLogado() {
        DatabaseHandler db = new DatabaseHandler(getBaseContext());
        if (CommonMethodsFragments.getDetails("displayname", db) == null || CommonMethodsFragments.getDetails("firebaseEmail", db).length() == 0) {
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        } else {
            reduceAdmincount();
            userModel = new UserModel(CommonMethodsFragments.getDetails("firebaseDisplayname", db), CommonMethodsFragments.getDetails("firebaseEmail", db));
            lerMessagensFirebase();
        }
    }

    /**
     * Vincular views com Java API
     */
    private void bindViews() {
        contentRoot = findViewById(R.id.contentRoot);
        edMessage = (EmojiconEditText) findViewById(R.id.editTextMessage);
        btSendMessage = (ImageView) findViewById(R.id.buttonMessage);
        gallery = (ImageView) findViewById(R.id.gallery);
        camera = (ImageView) findViewById(R.id.camera);
        location = (ImageView) findViewById(R.id.location);
        attach = (ImageView) findViewById(R.id.attach);
        img_pdf = findViewById(R.id.img_pdf);
        btSendMessage.setOnClickListener(this);
        gallery.setOnClickListener(this);
        camera.setOnClickListener(this);
        location.setOnClickListener(this);
        attach.setOnClickListener(this);
        img_pdf.setOnClickListener(this);
        btEmoji = (ImageView) findViewById(R.id.buttonEmoji);
        emojIcon = new EmojIconActions(this, contentRoot, edMessage, btEmoji);
        emojIcon.ShowEmojIcon();
        rvListMessage = (RecyclerView) findViewById(R.id.messageRecyclerView);
        mLinearLayoutManager = new LinearLayoutManager(this);
        mLinearLayoutManager.setStackFromEnd(true);
    }

    /**
     * Sign Out no login
     */
    private void signOut() {
        mFirebaseAuth.signOut();
        Auth.GoogleSignInApi.signOut(mGoogleApiClient);
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    /**
     * Checks if the app has permission to write to device storage
     * <p>
     * If the app does not has permission then the user will be prompted to grant permissions
     */
    public void verifyStoragePermissions() {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(ChatActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    ChatActivity.this,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        } else {
            // we already have permission, lets go ahead and call camera intent
            photoCameraIntent();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case REQUEST_EXTERNAL_STORAGE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted
                    photoCameraIntent();
                }
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonMessage:
                sendMessageFirebase();
                break;

            case R.id.gallery:
                photoGalleryIntent();
                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                break;

            case R.id.camera:
                verifyStoragePermissions();
                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                break;

            case R.id.location:
                locationPlacesIntent();
                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                break;

            case R.id.img_pdf:
                fileGalleryIntent();
                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                break;


            case R.id.attach:
                if (behavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                } else {
                    behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }
                break;
        }
    }


    private void fileGalleryIntent() {
        Intent intent = new Intent();
        intent.setType("application/pdf");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "SELECT FILE"), PICK_FILE_REQUEST);
    }

    private void uploadImage(StorageReference storageReference, Uri selectedImageUri) {

        final String name = DateFormat.format("yyyy-MM-dd_hhmmss", new Date()).toString();
        StorageReference fileRef = storageReference.child(name + "_pdf");

        rotateLoading.setVisibility(View.VISIBLE);
        rotateLoading.start();

        UploadTask uploadTask = fileRef.putFile(selectedImageUri);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.e(TAG, "onFailure sendFileFirebase " + e.getMessage());
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                if (rotateLoading.isStart()) {
                    rotateLoading.stop();
                    rotateLoading.setVisibility(View.GONE);
                }

                Log.i(TAG, "onSuccess sendFileFirebase");
                Uri downloadUrl = taskSnapshot.getDownloadUrl();
                FileModel fileModel = new FileModel("pdf", downloadUrl.toString(), name, "");
                ChatModel chatmodel = new ChatModel("media", "", "", userModel.getEmail(), "pdf",
                        name, downloadUrl.toString(), "", "123", Calendar.getInstance().getTime().getTime());
                mFirebaseDatabaseReference.child(CHAT_REFERENCE).child("Admin_" + userModel.getName()).push().setValue(chatmodel);
                increaseCount();
                reduceAdmincount();
            }
        });

    }

    private void increaseCount() {
        final Firebase reference = new Firebase("https://itwtravelgo-3bb51.firebaseio.com/users");
        reference.addListenerForSingleValueEvent(new com.firebase.client.ValueEventListener() {
            @Override
            public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {

                for (com.firebase.client.DataSnapshot snap : dataSnapshot.getChildren()) {
                    if (snap.child("email").getValue().equals(PrefUtils.getDefaults("email", getBaseContext()))) {
                        int i = Integer.parseInt(snap.child("count").getValue() + "") + 1;
                        reference.child(PrefUtils.getDefaults("displayname", getBaseContext())).child("count").setValue(i);
                        reference.child(PrefUtils.getDefaults("displayname", getBaseContext())).child("lastmsgtime").setValue(Calendar.getInstance().getTime().getTime());
                        // Toast.makeText(getBaseContext(), snap.child("count").getValue() + "", Toast.LENGTH_LONG).show();
                        Log.e(snap.getKey(), snap.getChildrenCount() + "");
                    }
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }


        });
    }

    private void reduceAdmincount() {
        final Firebase reference = new Firebase("https://itwtravelgo-3bb51.firebaseio.com/users");
        reference.addListenerForSingleValueEvent(new com.firebase.client.ValueEventListener() {
            @Override
            public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {

                for (com.firebase.client.DataSnapshot snap : dataSnapshot.getChildren()) {
                    if (snap.child("email").getValue().equals(PrefUtils.getDefaults("email", getBaseContext()))) {
                        reference.child(PrefUtils.getDefaults("displayname", getBaseContext())).child("admincount").setValue(0);
                        Log.e(snap.getKey(), snap.getChildrenCount() + "");
                    }
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }


        });
    }
}
